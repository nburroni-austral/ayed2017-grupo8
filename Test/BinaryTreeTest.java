import TP7ArbolesBinarios.BinaryTree;
import TP7ArbolesBinarios.BinaryTreeDisk;
import TP7ArbolesBinarios.Ejercicio13;
import TP7ArbolesBinarios.Ejercicio14;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;
/**
 * Created by Lucas on 9/4/2017.
 */
public class BinaryTreeTest {

    BinaryTree<Integer> leftRoot = new BinaryTree(2);
    BinaryTree<Integer> rightRootLeaveR = new BinaryTree(5);
    BinaryTree<Integer> rightRootLeaveL = new BinaryTree(4);
    BinaryTree<Integer> rightRoot = new BinaryTree(3,rightRootLeaveL,rightRootLeaveR);
    BinaryTree principalRoot = new BinaryTree(1,leftRoot,rightRoot);


    BinaryTree<Integer> root32R1L = new BinaryTree<Integer>(13);
    BinaryTree<Integer> root32R1R = new BinaryTree<Integer>(12);
    BinaryTree<Integer> root3L2R1R = new BinaryTree<Integer>(11);
    BinaryTree<Integer> root3L2L1R = new BinaryTree<Integer>(10);
    BinaryTree<Integer> root3R2R1L = new BinaryTree<Integer>(9);
    BinaryTree<Integer> root3L2L1L = new BinaryTree<Integer>(8);
    BinaryTree<Integer> root2R1R = new BinaryTree<Integer>(7, root32R1R, new BinaryTree<Integer>(15));
    BinaryTree<Integer> root2L1R = new BinaryTree<Integer>(6, root3L2L1R, root3L2R1R);
    BinaryTree<Integer> root2R1L = new BinaryTree<Integer>(5, root32R1L, new BinaryTree<Integer>(14));
    BinaryTree<Integer> root2L1L = new BinaryTree<Integer>(4, root3L2L1L, root3R2R1L);
    BinaryTree<Integer> root1R = new BinaryTree<Integer>(3, root2L1R, root2R1R);
    BinaryTree<Integer> root1L = new BinaryTree<Integer>(2, root2L1L, root2R1L);
    BinaryTree<Integer> root0 = new BinaryTree<Integer>(1, root1L, root1R);


    /**
     * The followings tests are for exercise 13.
     */
    @Test
    public void weigthTest(){
        assertEquals(5,Ejercicio13.weight(principalRoot));
    }

    @Test
    public void quantityLeavesTest(){
        assertEquals(3, Ejercicio13.quantityLeaves(principalRoot));
    }

    @Test
    public void quantityElementTest(){
        assertEquals(1,Ejercicio13.quantityElement(principalRoot,3));
    }

    @Test
    public void heightTest(){
        assertEquals( 3 , Ejercicio13.height(root0));
    }

    @Test
    public void quiantityElementsInLevelTest(){
        assertEquals(1, Ejercicio13.quantityOfElementsInLevel(principalRoot, 0));
        assertEquals(2, Ejercicio13.quantityOfElementsInLevel(principalRoot, 1));
        assertEquals(2, Ejercicio13.quantityOfElementsInLevel(principalRoot, 2));
    }

    /**
     * The following tests are for exercise 14.
     */

    @Test
    public void additionIntBinaryTree(){
        assertEquals(15, Ejercicio14.additionIntBinaryTree(principalRoot));
    }

    @Test
    public void additionIntBinaryTreeMultipleOfThree(){
        assertEquals(3, Ejercicio14.additionIntMultpleThreeBinaryTree(principalRoot));
        assertFalse(1==Ejercicio14.additionIntMultpleThreeBinaryTree(principalRoot));
    }

    @Test
    public void equalsTest(){
        assertTrue(Ejercicio14.equals(principalRoot,principalRoot));
    }

    @Test
    public void completeTest(){
        assertTrue(Ejercicio14.complete(principalRoot));


        BinaryTree<Integer> leftRoot2 = new BinaryTree(2);
        BinaryTree<Integer> rightRootLeaveR2 = new BinaryTree(5);
        BinaryTree<Integer> rightRoot2 = new BinaryTree(3,new BinaryTree(),rightRootLeaveR2);
        BinaryTree principalRoot2 = new BinaryTree(1,leftRoot2,rightRoot2);

        assertFalse(Ejercicio14.complete(principalRoot2));

    }

    @Test
    public void estableTest(){
        assertFalse(Ejercicio14.estable(principalRoot));

        BinaryTree<Integer> root = new BinaryTree<Integer>(1);

        assertTrue(Ejercicio14.estable(root));
    }

    @Test
    public void isomorfoTest(){
        assertTrue(Ejercicio14.isomorfos(principalRoot,principalRoot));


        BinaryTree<Integer> leftRoot3 = new BinaryTree(10);
        BinaryTree<Integer> rightRootLeaveR3 = new BinaryTree(6);
        BinaryTree<Integer> rightRootLeaveL3 = new BinaryTree(9);
        BinaryTree<Integer> rightRoot3 = new BinaryTree(8,rightRootLeaveL3,rightRootLeaveR3);
        BinaryTree principalRoot3 = new BinaryTree(7,leftRoot3,rightRoot3);

        assertTrue(Ejercicio14.isomorfos(principalRoot,principalRoot3));

    }

    @Test
    public void ocurrenceBinaryTree(){
        BinaryTree<Integer> rightRootLeaveROcurrence = new BinaryTree(5);
        BinaryTree<Integer> rightRootLeaveLOcurrence = new BinaryTree(4);
        BinaryTree<Integer> rootOcurrence = new BinaryTree(3,rightRootLeaveLOcurrence,rightRootLeaveROcurrence);

        assertTrue(Ejercicio14.ocurrenceBinaryTree(principalRoot,rootOcurrence));

        assertFalse(Ejercicio14.ocurrenceBinaryTree(root0,principalRoot));

        assertFalse(Ejercicio14.ocurrenceBinaryTree(root0,rootOcurrence));
    }

    @Test
    public void semejanteTest(){
        assertTrue(Ejercicio14.semejantes(principalRoot,principalRoot));

        BinaryTree<Integer> secondaryLeftRoot = new BinaryTree(1);
        BinaryTree<Integer> secondaryRightRootLeaveR = new BinaryTree(3);
        BinaryTree<Integer> secondaryRightRootLeaveL = new BinaryTree(4);
        BinaryTree<Integer> secondaryRightRoot = new BinaryTree(5,secondaryRightRootLeaveL,secondaryRightRootLeaveR);
        BinaryTree secondaryRoot = new BinaryTree(2,secondaryLeftRoot,secondaryRightRoot);

        assertTrue(Ejercicio14.semejantes(principalRoot,secondaryRoot));



    }

    @Test
    public void mostrarFronteraTest(){
        Ejercicio14.mostrarFrontera(root0);
    }

    @Test
    public void fullTest(){
       assertTrue(Ejercicio14.full(root0));
    }

    @Test
    public void readWriteTest() throws IOException, ClassNotFoundException {
        Ejercicio14.mostrarFrontera(root0);
        BinaryTreeDisk.record(root0);
        BinaryTree tree = BinaryTreeDisk.recover();
        Ejercicio14.mostrarFrontera(tree);
    }

}
