import TP7ArbolesBinarios.BinaryTree;
import TP7ArbolesBinarios.ShowBinaryTree;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Lucas on 10/4/2017.
 */
public class ShowBinaryTreeTest {
    BinaryTree<Integer> rightRoot = new BinaryTree(3);
    BinaryTree<Integer> leftRootLeaveR = new BinaryTree(5);
    BinaryTree<Integer> leftRootLeaveL = new BinaryTree(4);
    BinaryTree<Integer> leftRoot = new BinaryTree(2,leftRootLeaveL,leftRootLeaveR);
    BinaryTree principalRoot = new BinaryTree(1,leftRoot,rightRoot);


    @Test
    public void preorderTest(){
        ArrayList arrayPreorder = ShowBinaryTree.byPreorder(principalRoot);

        assertEquals(1,arrayPreorder.get(0));
        assertEquals(2,arrayPreorder.get(1));
        assertEquals(4,arrayPreorder.get(2));
        assertEquals(5,arrayPreorder.get(3));
        assertEquals(3,arrayPreorder.get(4));

    }

    @Test
    public void inorderTest(){
        ArrayList arrayInorder = ShowBinaryTree.byInorder(principalRoot);

        assertEquals(4,arrayInorder.get(0));
        assertEquals(2,arrayInorder.get(1));
        assertEquals(5,arrayInorder.get(2));
        assertEquals(1,arrayInorder.get(3));
        assertEquals(3,arrayInorder.get(4));
    }

    @Test
    public void postorderTest(){
        ArrayList arrayPostorder = ShowBinaryTree.byPostorder(principalRoot);

        assertEquals(4,arrayPostorder.get(0));
        assertEquals(5,arrayPostorder.get(1));
        assertEquals(2,arrayPostorder.get(2));
        assertEquals(3,arrayPostorder.get(3));
        assertEquals(1  ,arrayPostorder.get(4));
    }

    @Test
    public void levelTest(){
        ArrayList arrayLevel = ShowBinaryTree.byLevel(principalRoot);

        assertEquals(1,arrayLevel.get(0));
        assertEquals(2,arrayLevel.get(1));
        assertEquals(3,arrayLevel.get(2));
        assertEquals(4,arrayLevel.get(3));
        assertEquals(5,arrayLevel.get(4));

    }
}
