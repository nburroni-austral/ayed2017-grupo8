import TP4Cola.Exercise1.DynamicQueue;
import TP4Cola.Exercise1.Palindrome;
import  org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Lucas on 9/4/2017.
 */
public class QueueTest {
    DynamicQueue<Integer> queue = new DynamicQueue<Integer>();
//    StaticQueue<Integer> queue = new StaticQueue<Integer>();



    @Test
    public void enqueueAndDequeueTest(){
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);

        assertEquals((Integer) 1, queue.dequeue());
        assertEquals((Integer) 2, queue.dequeue());
        assertEquals((Integer) 3, queue.dequeue());
    }
    @Test
    public void palindromeTest(){
        assertTrue(Palindrome.palindrome2("Neuquen"));
        System.out.println("Passed");
        assertFalse(Palindrome.palindrome2("lucas"));
        System.out.println("Passed");
        assertTrue(Palindrome.palindrome2("Adan o nada"));
        System.out.println("Passed");
        assertTrue(Palindrome.palindrome2("A Toyota! Race fast, safe car! A Toyota!"));
        System.out.println("Passed");
    }
}
