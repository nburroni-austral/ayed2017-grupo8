package TP8ArbolesBusqueda;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Lucas on 21/4/2017.
 */
public class StockTest {
    Stock stock = new Stock();

    Lamp lucas = new Lamp("lucas","0",12,5);
    Lamp santiago = new Lamp("santiago","1",12,5);
    Lamp franco = new Lamp("franco","2",12,5);


    @Test
    public void addLamp() throws Exception{
        stock.addLamp(santiago);
        assertTrue(stock.getStock().ocurr(santiago));

        stock.addLamp(lucas);
        assertTrue(stock.getStock().ocurr(lucas));

        stock.addLamp(franco);
        assertTrue(stock.getStock().ocurr(franco));

    }

    @Test
    public void lookForLamp() throws Exception {
        stock.addLamp(santiago);
        assertEquals(santiago,stock.lookForLamp(santiago.getCodeLamp()));

        stock.addLamp(lucas);
        stock.addLamp(franco);

        assertEquals(santiago,stock.lookForLamp(santiago.getCodeLamp()));
        assertEquals(lucas,stock.lookForLamp(lucas.getCodeLamp()));
        assertEquals(franco,stock.lookForLamp(franco.getCodeLamp()));

        assertEquals(null,stock.lookForLamp("falseCode"));

        assertEquals(lucas,stock.lookForLamp("LuCaS"));
    }

    @Test
    public void getLamp()throws Exception{
        stock.addLamp(santiago);
        stock.getLamp("santiago");

        assertEquals(4, stock.lookForLamp("santiago").getQuantity());
    }

    @Test
    public void cleanLampFromStock() throws Exception {
        stock.addLamp(santiago);
        stock.cleanLampFromStock("santiago");

        assertTrue(stock.getStock().isEmpty());
    }
}