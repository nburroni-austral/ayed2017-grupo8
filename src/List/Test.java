package List;

import java.util.Scanner;

/**
 * Created by santiagohazana on 4/19/17.
 */
public class Test {

    public static void main(String[] args) {
        testProgram();
    }

    private static void testProgram() {
        Scanner sn = new Scanner(System.in);
        DynamicList<Integer> list = new DynamicList<Integer>();

        System.out.println("\nEnter a number");
        int i = sn.nextInt();
        while(i != 0){
            list.insertNext(i);
            System.out.println("Enter another number");
            i = sn.nextInt();
        }

        int result = 0;
        list.goTo(0);
        for (int j = 0; j < list.size(); j++) {
            result += list.getActual();
            if(list.endList())break;
            list.goNext();
        }
        System.out.println("The result is: " + result);
        testProgram();
    }
}
