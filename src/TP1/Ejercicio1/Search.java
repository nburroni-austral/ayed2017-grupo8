package TP1.Ejercicio1;

/**
 * Created by Lucas on 13/3/2017.
 */
public class Search<T>{

    /**
     * Metodo de busqueda secuencial, se le pasa un arreglo y el elemento a buscar,
     * que puede pertenecer o no al arreglo
     */

    @SuppressWarnings("unchecked")
    public Comparable secuencialSearch(Comparable<T> a[], Comparable<T> k){
        for (int i=0; i<a.length;i++){
            if(a[i].compareTo((T)k)==0)
                return a[i];
        }
        return null;
    }

    /**
     * Metodo de busqueda binaria, se le pasa un arreglo y un elemento a buscar,
     * que puede pertenecer o no al arreglo. El arreglo debe estar ordenado.
     */

    public Comparable binarySearch(Comparable<T> a[], Comparable<T> k){
        int min = 0;
        int max = a.length-1;
        int medio = (min+max)/2;
        while(min <= max) {

            if (k.compareTo((T) a[medio])== 0)
                return k;
            else if (k.compareTo((T) a[medio]) < 0)
                max = medio-1;
            else
                min = medio +1;
            medio = (min+max)/2;
        }
        return null;
    }
}
