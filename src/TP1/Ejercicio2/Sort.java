package TP1.Ejercicio2;



/**
 * Created by Lucas on 13/3/2017.
 */
public class Sort {

    /**
     * Seleccion:
     * Algoritmo que busca el menor el elemento de la lista
     * intercaladolo con el primer elemento.
     * Busca el siguiente menor de la lista y
     * intercala con el segundo elemento.
     * Repite n veces (tamaño del arreglo)
     */

    @SuppressWarnings("unchecked")
    public void selection(int[] a) {
        int min;
        int imin;
        for (int i = 0; i < a.length - 1; i++) {
            min = a[i];
            imin = i;
            for (int j = i + 1; j < a.length; j++) {
                if ((a[j]) < min) {
                    min = a[j];
                    imin = j;
                }
            }
            a[imin] = a[i];
            a[i] = min;
        }
    }

    public void selection(String[] a) {
        String min;
        int imin;
        for (int i = 0; i < a.length - 1; i++) {
            min = a[i];
            imin = i;
            for (int j = i + 1; j < a.length; j++) {
                if ((a[j]).compareTo(min) < 0) {
                    min = a[j];
                    imin = j;
                }
            }
            a[imin] = a[i];
            a[i] = min;
        }
    }

    public <T> void selection(Comparable<T> a[]) {
        Comparable<T> min;
        int imin;
        for (int i = 0; i < a.length - 1; i++) {
            min = a[i];
            imin = i;
            for (int j = i + 1; j < a.length; j++) {
                if ((a[j]).compareTo((T) min) < 0) {
                    min = a[j];
                    imin = j;
                }
            }
            a[imin] = a[i];
            a[i] = min;
        }
    }

    /**
     * Inserción:
     * Este algoritmo arranca desde la segunda posicion comparandolo con el de la primera,
     * y despues verifica cual es mayor para desplazarlo hacia la derecha.
     * Ahora desde la tercera posicion compara con los anteriores, y verifica si es mayor que algunos de los anteriores
     * para desplazarlo donde corresponda(Si no es mayor a ninguno queda como primero, si es mayor por lo menos a uno entonces
     * queda en el medio y si es mayor a ambos queda tercero).
     * Repeti hasta llegar al final del arreglo.
     */

    @SuppressWarnings("unchecked")
    public void insertion(int[] a) {
        int aux;
        for (int i = 1; i < a.length; i++) {
            aux = a[i];
            for (int j = i - 1; j >= 0; j--) {
                if (a[j] > aux) {
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
            }
        }
    }

    public void insertion(String[] a) {
        String aux;
        for (int i = 1; i < a.length; i++) {
            aux = a[i];
            for (int j = i - 1; j >= 0; j--) {
                if (a[j].compareTo(aux) > 0) {
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
            }
        }
    }

    public <T> void insertion(Comparable<T> a[]) {
        Comparable<T> aux;
        for (int i = 1; i < a.length; i++) {
            aux = a[i];
            for (int j = i - 1; j >= 0; j--) {
                if (a[j].compareTo((T) aux) > 0) {
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
            }
        }
    }

    /**
     * Burbujeo:
     * Algoritmo que toma un sub-intervalo de posiciones continuas de un arreglo que compara cual de todos es mayor para
     * acomodarlo hacia los derecha del sub-intervalo.
     * Es decir, empieza tomando las dos primeras posiciones y compara. El mayor queda en la segunda posicion (en la derecha).
     * Luego repeti extendiendo el sub-intervalo con el elemento de la tercera posicion y repite el ordenamiento.
     * Repetir para todos los elementos del arreglo.
     */

    @SuppressWarnings("unchecked")
    public void bubble(int[] a) {
        int i;
        int j;
        int aux;
        for (i = 0; i < a.length - 1; i++)
            for (j = 0; j < a.length - i - 1; j++)
                if (a[j + 1] < a[j]) {
                    aux = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
    }

    public void bubble(String[] a) {
        int i;
        int j;
        String aux;
        for (i = 0; i < a.length - 1; i++)
            for (j = 0; j < a.length - i - 1; j++)
                if (a[j + 1].compareTo(a[j]) < 0) {
                    aux = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
    }

    public <T> void bubble(Comparable<T> a[]) {
        int i;
        int j;
        Comparable<T> aux;
        for (i = 0; i < a.length - 1; i++)
            for (j = 0; j < a.length - i - 1; j++)
                if (a[j + 1].compareTo((T) a[j]) < 0) {
                    aux = a[j + 1];
                    a[j + 1] = a[j];
                    a[j] = aux;
                }
    }


    public <T> void recursiveSelection(Comparable<T> a[], int initArrayPosition) {
        Comparable<T> min;
        int imin;

        if (initArrayPosition < a.length - 1) {
            min = a[initArrayPosition];
            imin = initArrayPosition;

            for(int j = initArrayPosition + 1; j < a.length; j++) {
                if (a[j].compareTo((T) min)<0) {
                    min = a[j];
                    imin = j;
                }
            }
            a[imin] = a[initArrayPosition];
            a[initArrayPosition] = min;

            recursiveSelection(a,initArrayPosition+1);
        }
    }

    public void recursiveSelection(int[] a, int initArrayPosition) {
        int min;
        int imin;

        if (initArrayPosition < a.length - 1) {
            min = a[initArrayPosition];
            imin = initArrayPosition;

            for(int j = initArrayPosition + 1; j < a.length; j++) {
                if (a[j] < min) {
                    min = a[j];
                    imin = j;
                }
            }
            a[imin] = a[initArrayPosition];
            a[initArrayPosition] = min;

            recursiveSelection(a,initArrayPosition+1);
        }

    }
}
