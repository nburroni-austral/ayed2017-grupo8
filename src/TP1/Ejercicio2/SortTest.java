package TP1.Ejercicio2;

import java.util.Random;

/**
 * Created by Lucas on 14/3/2017.
 */
public class SortTest {
    public static void main(String[] args){
        Sort sort = new Sort();

        int[] a = new int[5];
        int[] b = new int[5];
        int[] c = new int[5];

        assignRandomInt(a,20);
        assignRandomInt(b,30);
        assignRandomInt(c,10);

        print(a,"Array 'a' not sorted: ");

        sort.selection(a);
        sort.insertion(b);
        sort.bubble(c);

        print(a,"Array 'a' sorted with 'selection':");
        print(b,"Array 'b' sorted with 'insertion':");
        print(c,"Array 'a' sorted with 'burbujeo':");

        int[] d = new int[5];

        assignRandomInt(d,10);
        sort.recursiveSelection(d,0);
        print(d,"Array 'd' sorted with 'recursiveSelection':");


    }

    public static void assignRandomInt(int[] x, int rangoTope){
        Random random = new Random();
        for (int i =0; i<x.length ; i++){
            x[i]=random.nextInt(rangoTope);
        }
    }

    public static void print(int[] x,String string){
        System.out.println("-----------"+"\n"+string);
        for (int i =0; i<x.length ; i++){
            System.out.println(x[i]);
        }
    }
}
