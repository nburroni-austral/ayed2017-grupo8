package TP1.Ejercicio3;



/**
 * Created by Lucas on 13/3/2017.
 */
public class Merge<T>{

    public Comparable<T>[] merge(Comparable<T> a[], Comparable<T> b[]) {

        Comparable<T> c[] = new Comparable[a.length + b.length];

        int ai = 0;
        int bi = 0;
        int ci;

        for (ci = 0; ci < c.length; ci++) {
            if(ai < a.length && bi < b.length){
                if (a[ai].compareTo((T) b[bi]) < 0) {
                    c[ci] = a[ai];
                    ai++;
                } else if (a[ai].compareTo((T) b[bi]) > 0) {
                    c[ci] = b[bi];
                    bi++;
                } else {
                    c[ci] = a[ai];
                    ci++;
                    c[ci] = b[bi];
                    ai++;
                    bi++;
                }
            } else if(!(ai < a.length)){
                c[ci] = b[bi];
                bi++;
            } else if(!(bi < b.length)){
                c[ci] = a[ai];
                ai++;
            }
        }

        return c;
    }
}
