package TP1;

import TP1.Ejercicio1.Search;
import TP1.Ejercicio2.Sort;
import TP1.Ejercicio3.Merge;

import java.util.Random;

/**
 * Created by santiagohazana on 3/13/17.
 */
public class Main {

    public static void main(String[] args) {

        Random rn = new Random();

        int[] random = new int[13];

        for (int i = 0; i < random.length; i++) {
            random[i] = rn.nextInt(20);
            System.out.println(random[i]);
        }


        Comparable<Integer>[] a = new Comparable[10];
        for (int i = 0; i < a.length; i++) {
            a[i]= i;
        }

        System.out.println("Array a:");

        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }

        Search search = new Search();

        System.out.println("Busquedas:");

        System.out.println(search.binarySearch(a, 1));
        System.out.println(search.secuencialSearch(a, 7));


        Comparable<Integer>[] b = new Comparable[10];

        b[0] = 4;
        b[1] = 2;
        b[2] = 9;
        b[3] = 3;
        b[4] = 11;
        b[5] = 5;
        b[6] = 17;
        b[7] = 6;
        b[8] = 1;
        b[9] = 0;

        Sort sort = new Sort();

        sort.insertion(b);

        System.out.println("");
        System.out.println("Array b:");

        for (int i = 0; i < b.length; i++) {
            System.out.println(b[i]);
        }

        System.out.println("");
        System.out.println("Merge:");

        Merge merge = new Merge();

        Comparable<Integer>[] c = merge.merge(a, b);


        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
        }

    }

}
