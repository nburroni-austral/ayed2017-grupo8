package TP10;

import java.io.*;

/**
 * Created by Lucas on 15/6/2017.
 */
public class Exercise1 {

    public static int numberOfCharacters(File file){
        int count = 0;
        try {
            FileReader fileReader = new FileReader(file);
            int character = fileReader.read();

            while (character!= -1){
                count++;
                character = fileReader.read();
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
    public static int numberOfLines(File file){
        int count = 0;
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = bufferedReader.readLine();
            while (str!= null){
                count++;
                str = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
