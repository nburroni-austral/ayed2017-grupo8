package TP10;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Lucas on 15/6/2017.
 */
public class Exercise2 {
    public static int numberOfOccurrence(File file, char c){
        int count = 0;

        try {
            FileReader fileReader = new FileReader(file);
            int character = fileReader.read();
            while (character != -1){
                if ((int) c == character){
                    count++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }
}
