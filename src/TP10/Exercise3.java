package TP10;

import java.io.*;

/**
 * Created by Lucas on 17/6/2017.
 */
public class Exercise3 {

    public static void changeUpperLowerCase(File file , boolean  toUpperCase){
        try {
            FileReader fileReader = new FileReader(file);
            FileWriter fileWriter = new FileWriter("Exercise3");

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String str = bufferedReader.readLine();

            while (str != null){
                if (toUpperCase)
                    str.toUpperCase();
                else
                    str.toLowerCase();
                fileWriter.write(str+"\n");
                str = bufferedReader.readLine();
            }
            fileReader.close();
            fileWriter.close();
        } catch (IOException e) {
        e.printStackTrace();
        }
    }
}
