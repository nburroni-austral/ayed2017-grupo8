package TP10;

import java.io.*;

/**
 * Created by Lucas on 17/6/2017.
 */
public class Exercise5 {
    public static void divideCountries(File file){
        try {
            FileReader fileReader = new FileReader(file);
            FileWriter countryMore30M = new FileWriter("CountryWithMore30M");
            FileWriter countryLess30M = new FileWriter("CountryWithLess30M");

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = bufferedReader.readLine();
            while (str!= null) {
                int numberPopulation = Integer.parseInt(str.substring(30,40)); //Assume that the first 30 characters are for Country's name, the next 10 character for population (complete with 0's if it's less)
                if (numberPopulation<30000000)
                    countryLess30M.write(str+"\n");
                else
                    countryMore30M.write(str+"\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void divideCountries(File file, boolean PBIInsteadPopulation){
        try {
            FileReader fileReader = new FileReader(file);
            FileWriter countryMore30M = new FileWriter("CountryWithMore30M");
            FileWriter countryLess30M = new FileWriter("CountryWithLess30M");

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String str = bufferedReader.readLine();

            if (PBIInsteadPopulation) {
                while (str != null) {
                    int numberPopulation = Integer.parseInt(str.substring(30, 40)); //Assume that the first 30 characters are for Country's name, the next 10 character for population (complete with 0's if it's less)
                    if (numberPopulation < 30000000)
                        countryLess30M.write(str.substring(0,30)+str.substring(40)+"\n");
                    else
                        countryMore30M.write(str.substring(0,30)+str.substring(40)+"\n");
                }
            }else {
                while (str != null) {
                    int numberPopulation = Integer.parseInt(str.substring(30, 40)); //Assume that the first 30 characters are for Country's name, the next 10 character for population (complete with 0's if it's less)
                    if (numberPopulation < 30000000)
                        countryLess30M.write(str.substring(0,40) + "\n");
                    else
                        countryMore30M.write(str.substring(0,40) + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
