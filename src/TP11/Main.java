package TP11;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by santiagohazana on 7/8/17.
 */
public class Main {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Enter file name");
        String fileName = "students";
        menu(scanner, fileName);
        System.exit(0);
    }

    private static void menu(Scanner scanner, String fileName) throws IOException {
        int operate = -1;
        while(operate !=0) {
            StudentFile studentFile = new StudentFile(fileName);
            System.out.println("\nMain Menu: please select an option\n\n" +
                    "1. Add new student\n" +
                    "2. Unsubscribe student\n" +
                    "3. Modify student subjects\n" +
                    "4. See student info\n" +
                    "5. Number of students registered\n" +
                    "6. Number of students subscribed to more than 5 subjects\n" +
                    "7. Number of students subscribed to 5 or less subjects\n" +
                    "8. List of all students\n" +
                    "9. List of students subscribed to more than 5 subjects\n" +
                    "10. List of students enabled for finals\n" +
                    "11. Change student state for finals\n"+
                    "0. Exit system.\n");
            int option = scanner.nextInt();

            switch (option){
                case 1:
                    addStudent(scanner, studentFile);
                    break;
                case 2:
                    removeStudent(scanner, studentFile);
                    break;
                case 3:
                    changeStudentSubjects(scanner, studentFile);
                    break;
                case 4:
                    System.out.println("Enter student enrollment ID:");
                    seeStudentInfo(scanner.nextInt(), studentFile);
                    break;
                case 5:
                    System.out.println("Number of students = " + studentFile.studentsLength());
                    break;
                case 6:
                    System.out.println("Number of students subscribed to more than 5 subjects = " + studentFile.getStudentsValidForFinals());
                    break;
                case 7:
                    System.out.println("Number of students subscribed to 5 or less subjects = " + studentFile.getStudentsWithLessThan5Subjects());
                    break;
                case 8:
                    listAllStudents(studentFile.getAllStudents());
                    break;
                case 9:
                    listAllStudentsWithMoreThan5(studentFile.getAllStudents());
                    break;
                case 10:
                    listAllStudentsEnabledForFinals(studentFile.getAllStudents());
                    break;
                case 11:
                    changeFinalsState(scanner, studentFile);
                    break;
                case 0:
                    operate = 0;
                default:
                    break;
            }
        }
    }

    private static void addStudent(Scanner scanner, StudentFile studentFile){
        System.out.println("Enter student ID: \n");
        int id = scanner.nextInt();
        System.out.println("Enter enrollment ID: \n");
        int enrollmentID = scanner.nextInt();
        studentFile.addStudent(new Student(id, enrollmentID, 0, true, true));
        studentFile.closeFile();
    }

    private static void removeStudent(Scanner scanner, StudentFile studentFile) throws IOException {
        System.out.println("Enter student enrollment ID: \n");
        int enrollmentID = scanner.nextInt();
        int position = studentFile.getStudentPosition(enrollmentID);
        Student student = studentFile.searchForStudent(enrollmentID);
        student.setActive(false);
        studentFile.addStudent(student, position);
        studentFile.closeFile();
    }

    private static void changeStudentSubjects(Scanner scanner, StudentFile studentFile) throws IOException {
        System.out.println("Enter student enrollment ID: \n");
        int enrollmentID = scanner.nextInt();
        int position = studentFile.getStudentPosition(enrollmentID);
        Student student = studentFile.searchForStudent(enrollmentID);
        System.out.println("Enter number of subjects: \n");
        student.setNumberOfSubjectsSubscribed(scanner.nextInt());
        studentFile.addStudent(student, position);
    }

    private static void changeFinalsState(Scanner scanner, StudentFile studentFile) throws IOException {
        System.out.println("Enter student enrollment ID: \n");
        int enrollmentID = scanner.nextInt();
        int position = studentFile.getStudentPosition(enrollmentID);
        Student student = studentFile.searchForStudent(enrollmentID);
        System.out.println("Type true to enable or false to disable: \n");
        student.setValidForFinals(scanner.nextBoolean());
        studentFile.addStudent(student, position);
    }

    private static void seeStudentInfo(int enrollmentID, StudentFile studentFile) throws IOException {
        Student student = studentFile.searchForStudent(enrollmentID);
        System.out.println("Student ID: " + student.getId() + " EnrollmentID: " + student.getEnrollmentID() + " Number of subjects: " + student.getNumberOfSubjectsSubscribed()+
                " Is enabled for finals? " + student.isValidForFinals());
    }

    private static void listAllStudentsEnabledForFinals(ArrayList<Student> allStudents) {
        for (int i = 0; i < allStudents.size(); i++) {
            if(allStudents.get(i).isValidForFinals() && allStudents.get(i).isActive())
                System.out.println("Student ID: " + allStudents.get(i).getId() + " EnrollmentID: " + allStudents.get(i).getEnrollmentID() + " Number of subjects: " +
                        allStudents.get(i).getNumberOfSubjectsSubscribed() + " Is enabled for finals? " + allStudents.get(i).isValidForFinals() + "\n\n");
        }
    }

    private static void listAllStudentsWithMoreThan5(ArrayList<Student> allStudents) {
        for (int i = 0; i < allStudents.size(); i++) {
            if(allStudents.get(i).getNumberOfSubjectsSubscribed() > 5 && allStudents.get(i).isActive())
                System.out.println("Student ID: " + allStudents.get(i).getId() + " EnrollmentID: " + allStudents.get(i).getEnrollmentID() + " Number of subjects: " +
                        allStudents.get(i).getNumberOfSubjectsSubscribed() + " Is enabled for finals? " + allStudents.get(i).isValidForFinals() + "\n\n");
        }
    }

    private static void listAllStudents(ArrayList<Student> allStudents) {
        for (int i = 0; i < allStudents.size(); i++) {
            if(allStudents.get(i).isActive())
                System.out.println("Student ID: " + allStudents.get(i).getId() + " EnrollmentID: " + allStudents.get(i).getEnrollmentID() + " Number of subjects: " +
                    allStudents.get(i).getNumberOfSubjectsSubscribed() + " Is enabled for finals? " + allStudents.get(i).isValidForFinals() + "\n\n");
        }
    }

}
