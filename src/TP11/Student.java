package TP11;

/**
 * Created by santiagohazana on 7/8/17.
 */
public class Student {

    private int id;
    private int enrollmentID;
    private int numberOfSubjectsSubscribed;
    private boolean validForFinals;
    private boolean active;

    public  Student(){

    }

    public Student(int id, int enrollmentID, int numberOfSubjectsSubscribed, boolean validForFinals, boolean active) {
        this.id = id;
        this.enrollmentID = enrollmentID;
        this.numberOfSubjectsSubscribed = numberOfSubjectsSubscribed;
        this.validForFinals = validForFinals;
        this.active = active;
    }

    public void setNumberOfSubjectsSubscribed(int numberOfSubjectsSubscribed) {
        this.numberOfSubjectsSubscribed = numberOfSubjectsSubscribed;
    }

    public void setValidForFinals(boolean validForFinals) {
        this.validForFinals = validForFinals;
    }

    public void setActive(boolean active){
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public int getEnrollmentID() {
        return enrollmentID;
    }

    public int getNumberOfSubjectsSubscribed() {
        return numberOfSubjectsSubscribed;
    }

    public boolean isValidForFinals() {
        if(!isActive())
            validForFinals = false;

        return validForFinals;
    }

    public boolean isActive() {
        return active;
    }
}
