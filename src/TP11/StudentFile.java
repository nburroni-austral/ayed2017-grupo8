package TP11;

import com.sun.org.apache.bcel.internal.generic.ARRAYLENGTH;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/**
 * Created by santiagohazana on 7/9/17.
 */
public class StudentFile {

    private File file;
    private RandomAccessFile raf;
    private int studentByteLength = 14;

    public StudentFile(String name){
        file = new File(name);
        try {
            raf = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addStudent(Student student){
        try {
            raf.seek(fileLength());
            raf.writeInt(student.getId());
            raf.writeInt(student.getEnrollmentID());
            raf.writeInt(student.getNumberOfSubjectsSubscribed());
            raf.writeBoolean(student.isValidForFinals());
            raf.writeBoolean(student.isActive());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addStudent(Student student, int position) throws IOException {
        goTo(position);
        raf.writeInt(student.getId());
        raf.writeInt(student.getEnrollmentID());
        raf.writeInt(student.getNumberOfSubjectsSubscribed());
        raf.writeBoolean(student.isValidForFinals());
        raf.writeBoolean(student.isActive());
    }

    public void closeFile(){
        try {
            raf.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long fileLength() throws IOException {
        return raf.length();
    }

    public void goToStart() throws IOException {
        raf.seek(0);
    }

    public void goToEnd() throws IOException {
        raf.seek(fileLength());
    }

    public void goTo(int position) throws IOException {
        raf.seek((position)* studentByteLength);
    }

    public long studentsLength() throws IOException {
        return fileLength()/ studentByteLength;
    }

    private Student readStudent() throws IOException {
        return new Student(raf.readInt(), raf.readInt(), raf.readInt(), raf.readBoolean(), raf.readBoolean());
    }

    Student searchForStudent(int enrollmentID) throws IOException {
        goToStart();
        Student student;
        for (int i = 0; i < studentsLength(); i++) {
            student = readStudent();
            if(student.getEnrollmentID() == enrollmentID)
                return student;
        }
        System.out.println("Student not found");
        return new Student();
    }

    public int getStudentsValidForFinals() throws IOException {
        Student student;
        goToStart();
        int count = 0;
        for (int i = 0; i < studentsLength(); i++) {
            student = readStudent();
            if(student.isValidForFinals())
                count++;

        }
        return count;
    }

    public int getStudentsWithMoreThan5Subjects() throws IOException {
        Student student;
        goToStart();
        int count = 0;
        for (int i = 0; i < studentsLength(); i++) {
            student = readStudent();
            if(student.getNumberOfSubjectsSubscribed() > 5 && student.isActive())
                count++;

        }
        return count;
    }

    public int getStudentsWithLessThan5Subjects() throws IOException {
        Student student;
        goToStart();
        int count = 0;
        for (int i = 0; i < studentsLength(); i++) {
            student = readStudent();
            if(student.getNumberOfSubjectsSubscribed() <= 5 && student.isActive())
                count++;

        }
        return count;
    }

    public int getStudentPosition(int enrollmentID) throws IOException {
        int position= -1;
        Student student;
        for (int i = 0; i < studentsLength(); i++) {
            student = searchForStudent(enrollmentID);
            if(student.getEnrollmentID() == enrollmentID)
                position = i;
        }
        return position;
    }

    public ArrayList<Student> getAllStudents() throws IOException {
        goToStart();
        ArrayList students = new ArrayList<Student>();
        for (int i = 0; i < studentsLength(); i++) {
            Student student = readStudent();
            students.add(student);
        }
        return students;
    }


}
