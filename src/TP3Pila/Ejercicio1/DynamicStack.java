package TP3Pila.Ejercicio1;

import struct.istruct.Stack;

/**
 * Created by Lucas on 26/3/2017.
 */
public class DynamicStack<T> implements Stack<T> {
    private Node<T> top;
    int size=0;

    public DynamicStack() {
        this.top=null;
    }

    public void push(T t) {
        Node aux = new Node<T>(t);
        aux.next= top;
        top=aux;
        size++;
    }

    public void pop() {
        top=top.next;
        --size;
    }

    public T peek() {
        return top.getObject();
    }

    public boolean isEmpty() {
        if(size==0){
            return true;
        }
        return false;
    }

    public int size() {
        return size;
    }

    public void empty() {
        size=0;
        top=null;
    }
}
