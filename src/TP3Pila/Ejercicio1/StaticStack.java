package TP3Pila.Ejercicio1;

import struct.istruct.Stack;

/**
 * Created by Lucas on 26/3/2017.
 */
public class StaticStack<T> implements Stack<T>{
    private int top=-1;
    private int maxCapacity;
    private Object[] stack;

    public StaticStack(int maxCapacity){
        this.maxCapacity=maxCapacity;
        stack = new Object[maxCapacity];
    }

    public void push(T t) {
        if(top==maxCapacity)
            growArray();
        top++;
        stack[top]=t;
    }

    public void pop() {
        top--;
    }

    @SuppressWarnings("unchecked")
    public T peek() {
        if(!isEmpty()){
            return (T) stack[top];
        }
        return null;
    }

    public boolean isEmpty() {
        if(top==-1)
            return true;
        return false;
    }

    public int size() {
        return top;
    }

    public void empty() {
        top=-1;
    }

    private void growArray(){
        Object[] stack2= new Object[maxCapacity*2];
        for(int i=0; i<=maxCapacity ;i++){
            stack2[i]=stack[i];
        }
        stack = stack2;
    }
}
