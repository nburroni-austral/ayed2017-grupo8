package TP3Pila.Ejercicio2;

import TP3Pila.Ejercicio1.DynamicStack;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by santiagohazana on 3/28/17.
 */
public class  LexicographicAnalyzer {

    private static DynamicStack<String> brackets=new DynamicStack<String>();
    private static DynamicStack<String> parenthesis = new DynamicStack<String>();
    private static DynamicStack<String> squareBrackets = new DynamicStack<String>();

    private static int OPEN_PARENTHESIS_ASCII = 40;
    private static int CLOSE_PARENTHESIS_ASCII = 41;
    private static int OPEN_BRACKET_ASCII = 123;
    private static int CLOSE_BRACKET_ASCII = 125;
    private static int OPEN_SQUAREBRACKET_ASCII = 91;
    private static int CLOSE_SQUAREBRACKET_ASCII = 93;

    private static int BRACKETS_STACK_IDENTIFIER = 1;
    private static int PARENTHESIS_STACK_IDENTIFIER = 2;
    private static int SQUAREBRACKETS_STACK_IDENTIFIER = 3;

    private LexicographicAnalyzer(){}

    /**
     * Description: open a file given by a string containing the direction in disk
     */

    public static void openFile(String textFileDirection) throws IOException {
        BufferedReader buffer = new BufferedReader(new FileReader(textFileDirection));
        String bufferStrAnalize;

        while ((bufferStrAnalize = buffer.readLine() )!=null){
            analyzeLine(bufferStrAnalize);
        }
        buffer.close();
        evaluateStack(brackets, BRACKETS_STACK_IDENTIFIER);
        evaluateStack(parenthesis,PARENTHESIS_STACK_IDENTIFIER);
        evaluateStack(squareBrackets,SQUAREBRACKETS_STACK_IDENTIFIER);

    }

    /**
     * Description: called every time a line is read, stores each (), {} or [] in its respective stack and obviates anything else
     */

    private static void analyzeLine(String archive){
        for (int i = 0; i < archive.length(); i++) {
            if(archive.charAt(i) == OPEN_PARENTHESIS_ASCII || archive.charAt(i) == CLOSE_PARENTHESIS_ASCII){
                parenthesis.push(archive.substring(i, i+1));
            } else if (archive.charAt(i) == OPEN_SQUAREBRACKET_ASCII || archive.charAt(i) == CLOSE_SQUAREBRACKET_ASCII){
                squareBrackets.push(archive.substring(i, i+1));
            } else if (archive.charAt(i) == OPEN_BRACKET_ASCII || archive.charAt(i) == CLOSE_BRACKET_ASCII){
                brackets.push(archive.substring(i, i+1));
            }
        }
    }

    /**
     * Description: evaluates each stack and tells if any (), {} or [] is missing
     */

    private static void evaluateStack(DynamicStack<String> stack, int identifier){
        int open = 0;
        String opened = "";
        int close = 0;
        String closed = "";

        if(identifier == BRACKETS_STACK_IDENTIFIER){
            opened = "{";
            closed = "}";
        }else if(identifier == PARENTHESIS_STACK_IDENTIFIER){
            opened = "(";
            closed = ")";
        }else if(identifier == SQUAREBRACKETS_STACK_IDENTIFIER){
            opened = "[";
            closed = "]";
        }

        while(!stack.isEmpty()) {
            if(stack.peek().charAt(0) == OPEN_PARENTHESIS_ASCII || stack.peek().charAt(0) == OPEN_BRACKET_ASCII || stack.peek().charAt(0) == OPEN_SQUAREBRACKET_ASCII){
                stack.pop();
                open++;
            }else{
                stack.pop();
                close++;
            }
        }

        if(open < close){
            System.out.println("Missing " + (close-open) + " " + opened);
        }else if (open > close){
            System.out.println("Missing " + (open-close) + " " + closed);
        }else{
            System.out.println("Everything ok with " + opened + " " + closed);
        }
    }

}
