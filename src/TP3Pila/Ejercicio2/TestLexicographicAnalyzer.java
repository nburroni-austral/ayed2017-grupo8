package TP3Pila.Ejercicio2;

import java.io.IOException;

/**
 * Created by Lucas on 28/3/2017.
 */
public class TestLexicographicAnalyzer {
    public static void main(String[] args) throws IOException {
        System.out.println("First archive, everything ok\n");
        LexicographicAnalyzer.openFile("/Users/santiagohazana/Desktop/TXT1.txt");
        System.out.println("\n Second archive, missing some \n");
        LexicographicAnalyzer.openFile("/Users/santiagohazana/Desktop/TXT2.txt");
        System.out.println("\n Third archive, missing more\n");
        LexicographicAnalyzer.openFile("/Users/santiagohazana/Desktop/TXT3.txt");
    }
}