package TP3Pila.Ejercicio3;

/**
 * Created by santiagohazana on 3/26/17.
 */
public class Car {

    private String patente;
    private String color;
    private String model;

    public Car (String patente, String color, String model){
        this.patente = patente;
        this.color = color;
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public String getModel() {
        return model;
    }

    public String getPatente() {
        return patente;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
