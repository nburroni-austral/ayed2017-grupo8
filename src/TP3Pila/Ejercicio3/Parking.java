package TP3Pila.Ejercicio3;

import TP3Pila.Ejercicio1.StaticStack;

/**
 * Created by Lucas on 26/3/2017.
 */
public class Parking {
    private StaticStack<Car> parking;
    private int cashOfDay;

    public Parking() {
        this.parking = new StaticStack<Car>(50);
    }

    public void pushCar(Car aCar) {
        parking.push(aCar);
        cashOfDay += 5;
    }

    public Car getCar(Car aCar) {
        StaticStack<Car> sideWalk = new StaticStack<Car>(parking.size());
        Car aux;
        int maxSize = parking.size();
        for (int i = 0; i <= maxSize; i++) {
            aux = parking.peek();
            parking.pop();
            if (aux.equals(aCar)) {
                refillParking(sideWalk);
                return aux;
            }
            sideWalk.push(aux);
        }
        refillParking(sideWalk);
        return null;
    }

    private void refillParking(StaticStack<Car> sideWalk) {
        while (!sideWalk.isEmpty()) {
            parking.push(sideWalk.peek());
            sideWalk.pop();
        }
    }

    public int getCash() {
        int cash = cashOfDay;
        emptyParking();
        return cash;
    }

    private void emptyParking() {
        cashOfDay = 0;
        parking.empty();
    }
}

