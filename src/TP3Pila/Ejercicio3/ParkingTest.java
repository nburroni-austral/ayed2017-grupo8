package TP3Pila.Ejercicio3;

import TP3Pila.Ejercicio3.Car;
import TP3Pila.Ejercicio3.Parking;

/**
 * Created by santiagohazana on 3/27/17.
 */
public class ParkingTest {

    public static void main(String[] args) {
        Car car01 = new Car("AAA000", "white", "Gol");
        Car car02 = new Car("AAA001", "blue", "Porsche");
        Car car03 = new Car("AAA002", "yellow", "Uno");
        Car car04 = new Car("AAA003", "green", "City");
        Car car05 = new Car("AAA004", "black", "Civic");
        Car car06 = new Car("AAA005", "grey", "Agile");
        Car car07 = new Car("AAA006", "red", "Lamborghini");
        Car car08 = new Car("AAA007", "orange", "Ferrari");
        Car car09 = new Car("AAA008", "burgundy", "Up");
        Car car10 = new Car("AAA009", "black", "Vento");
        Car car11 = new Car("AAA010", "white", "Suran");
        Car car12 = new Car("AAA011", "blue", "Mercedes Benz");

        Parking parking = new Parking();

        parking.pushCar(car01);
        parking.pushCar(car02);
        parking.pushCar(car03);
        parking.pushCar(car04);
        parking.pushCar(car05);
        parking.pushCar(car06);
        parking.pushCar(car07);
        parking.pushCar(car08);
        parking.pushCar(car09);
        parking.pushCar(car10);
        parking.pushCar(car11);
        parking.pushCar(car12);

        parking.getCar(car05);
        parking.getCar(car11);
        parking.getCar(car08);

        System.out.println(parking.getCash());
    }

}
