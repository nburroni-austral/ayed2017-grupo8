package TP3Pila.Ejercicio4;

import TP3Pila.Ejercicio1.DynamicStack;

import java.util.Scanner;

/**
 * Created by Lucas on 27/3/2017.
 */
public class Calculator {
    private DynamicStack<String> memory;
    private DynamicStack<String> terms;
    private String operation;
    private double result;
    private String operator="";

    /**
     * Constructor, creates the calculator with two stacks, one for the memory and other one for the terms.
     */

    public Calculator() {
        System.out.println("Calculator 3500X\n\n Created by: \n -Lucas Manzanelli\n -Santiago Hazaña\n");
        this.memory = new DynamicStack<String>();
        terms= new DynamicStack<String>();
    }

        public void calculatorMain(){
            System.out.println("\n"+"1.Calculate operation "+"\n"+"2.Clean memory"+"\n"+"3.Check out last memory number"+"\n"+"0.Exit program"+"\n");
            System.out.println("Introduce a valid command:");
            Scanner command = new Scanner(System.in);
            int aCase =command.nextInt();
            switch (aCase){
                case 1:
                    calculate();
                    calculatorMain();
                    break;
                case 2:
                    memory.empty();
                    calculatorMain();
                    break;
                case 3:
                    if(memory.isEmpty())
                        System.out.println("Memory is already empty!");
                    else
                        System.out.println(memory.peek());
                    calculatorMain();
                    break;
                case 0:
                    System.exit(0);
                    break;

                default:
                    System.out.println("Please enter a correct command!");
                    calculatorMain();
                    break;

            }
        }

    /**
     * Calculate method, starts the calculator asking for an operation, calls all the necessary methods to show the result.
     */

    public void calculate(){
        operationByConsole();
        separateInTerms();
        termsToMemory();
        calculateFinalResult();
        memory.push(String.valueOf(result));
        System.out.println("Final result is: " + result);
        continueOperation();
    }

    private void continueOperation() {
        Scanner sn = new Scanner(System.in);
    }

    /**
     * Trims the operation string in all the terms and stores it in the terms stack.
     */

    private void termsToMemory(){
        while(!terms.isEmpty()){
            memory.push(analyzeTerm(terms.peek()));
            terms.pop();
            if(!terms.isEmpty()) {
                memory.push(terms.peek());
                terms.pop();
            }
        }
    }

    /**
     * Calculates the final result, using the memory stack that only has + and - operation
     */

    private void calculateFinalResult(){
        result = Double.parseDouble(memory.peek());
        memory.pop();

        while(!memory.isEmpty()){
            if(memory.peek().compareTo("+") == 0){
                memory.pop();
                result += Double.parseDouble(memory.peek());
                memory.pop();
            }else if (memory.peek().compareTo("-") == 0){
                memory.pop();
                result -= Double.parseDouble(memory.peek());
                memory.pop();
            }
        }
    }

    /**
     * Description: analyzes the term given and stores it in a temporary stack, then calls calculateTermValue passing the stack
     */

    private String analyzeTerm(String term){

            DynamicStack<String> subTerm = new DynamicStack<String>();
            int position = term.length();
            for (int i = term.length() - 1; i >= 0; i--) {
                if (term.charAt(i) == 42 || term.charAt(i) == 47) {
                    subTerm.push(term.substring(i+1, position));
                    subTerm.push(term.substring(i, i+1));
                    term = term.substring(0, i);
                    position = i;
                }
            }
            subTerm.push(term);
            return calculateTermValue(subTerm);
    }

    /**
     * Description: Given a term, calculates the value of that term, only for * and / operation
     */

    private String calculateTermValue(DynamicStack<String> subTerm){
        String result="";
        double numberTemp=0;
        while(!subTerm.isEmpty()){
            if(subTerm.peek().charAt(0) != 42 && subTerm.peek().charAt(0) != 47){
                numberTemp = Double.parseDouble(subTerm.peek());
                subTerm.pop();
            }else if(subTerm.peek().charAt(0) == 42){
                subTerm.pop();
                numberTemp = numberTemp * Double.parseDouble(subTerm.peek());
                subTerm.pop();
            }else{
                subTerm.pop();
                numberTemp = numberTemp / Double.parseDouble(subTerm.peek());
                subTerm.pop();
            }
        }
        result+=numberTemp;
        return result;

    }

    /**
     * Description: separates the operation string into terms and stores it in the terms stack
     */

    private void separateInTerms(){
        int position=0;
        for(int i=0; i<operation.length();i++){
            if(operation.charAt(i)==43 || operation.charAt(i)==45){
                terms.push(operation.substring(position,i));
                terms.push(operation.substring(i,i+1));
                position=i+1;
            }
        }
        terms.push(operation.substring(position,operation.length()));
    }

    /**
     * Description: asks for an operation to the user
     */

    private void operationByConsole(){
        Scanner strSc = new Scanner(System.in);
        System.out.println("Introduce an operation: ");
        operation =  strSc.nextLine();
//        analyzeOperation(operation);
    }
    private void checkAnsUseOperation(){

    }

//    /**
//     * Description: if the user entered elements that are not required for the calculation, it eliminates them,
//     * anything but numbers and operation symbols get eliminated.
//     */
//
//    @SuppressWarnings("ResultOfMethodCallIgnored")
//    private void analyzeOperation(String string){
//
//    }

}
