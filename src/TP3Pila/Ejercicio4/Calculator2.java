package TP3Pila.Ejercicio4;

import TP3Pila.Ejercicio1.DynamicStack;

import java.util.Scanner;

/**
 * Created by santiagohazana on 3/28/17.
 */
public class Calculator2 {
    private DynamicStack<String> memory;
    private DynamicStack<String> terms;
    private String operation;
    private double result = 0;

    private static int PLUS_OPERATOR_ASCII = 43;
    private static int SUBTRACTION_OPERATOR_ASCII = 45;
    private static int MULTIPLICATION_OPERATOR_ASCII = 42;
    private static int DIVISION_OPERATOR_ASCII = 47;
    private static int SPACE_ASCII = 32;
    private static int ZERO_ASCII = 48;
    private static int NINE_ASCII = 57;
    private static int STOP_ASCII = 46;


    /**
     * Constructor, creates the calculator with two stacks, one for the memory and other one for the terms.
     */

    public Calculator2() {
        System.out.println("Calculator 3500X. Version: alpha 4 (Ni en pedo terminada)\n\n Created by: \n -Lucas Manzanelli\n -Santiago Hazaña\n");
        this.memory = new DynamicStack<String>();
        terms= new DynamicStack<String>();
    }

    /**
     * Description: asks the user for an operation, and checks if it is necessary to use the last result.
     */

    private void calculate(){
        Scanner sc = new Scanner(System.in);
        operation = sc.nextLine();
        if((operation.charAt(0) == PLUS_OPERATOR_ASCII || operation.charAt(0) == SUBTRACTION_OPERATOR_ASCII ||
                operation.charAt(0) == MULTIPLICATION_OPERATOR_ASCII || operation.charAt(0) == DIVISION_OPERATOR_ASCII) &&
                !memory.isEmpty()){
            operation = memory.peek() + " " + operation;
            memory.empty();
        }
        separateInTerms(operation);
        calculateResult();
    }

    /**
     * Description: separates the operation string into terms and stores it in the terms stack
     */

    private void separateInTerms(String operation){
        int position = 0;
        for (int i = 1; i < operation.length(); i++) {
            if((operation.charAt(i) == PLUS_OPERATOR_ASCII || operation.charAt(i) == SUBTRACTION_OPERATOR_ASCII) && operation.charAt(i-1) == SPACE_ASCII && operation.charAt(i+1) == SPACE_ASCII){
                terms.push(operation.substring(position, i-1));
                terms.push(operation.substring(i, i+1));
                i++;
                position = i;
            }
        }
        terms.push(operation.substring(position));
        separateTerm();
    }

    /**
     * Description: separates each term and calls analyzeSubTerm, then pushes the result to memory
     */

    private void separateTerm(){
        String aux;
        while (!terms.isEmpty()){
            aux = terms.peek();
            analyzeSubTerm(aux);
            terms.pop();
            if(!terms.isEmpty()) {
                memory.push(terms.peek());
                terms.pop();
            }
        }

    }

    /**
     * Description: analyzes the term given and stores it in a temporary stack, then calls calculateTermValue passing the stack
     */

    private void analyzeSubTerm(String aux){
        DynamicStack<String> subTerm = new DynamicStack<String>();
        int size = aux.length();
        String term = "";
        for (int i = 0; i < size; i++) {
            if ((aux.charAt(i) >= ZERO_ASCII && aux.charAt(i) <= NINE_ASCII) || aux.charAt(i) == MULTIPLICATION_OPERATOR_ASCII ||
                    aux.charAt(i) == DIVISION_OPERATOR_ASCII || aux.charAt(i) == SUBTRACTION_OPERATOR_ASCII || aux.charAt(i) == STOP_ASCII) {
                term += aux.substring(i, i+1);
            }
        }
        int position = 0;
        for (int i = 0; i < term.length(); i++) {
            if(term.charAt(i) == MULTIPLICATION_OPERATOR_ASCII || term.charAt(i) == DIVISION_OPERATOR_ASCII){
                    subTerm.push(term.substring(position, i));
                    subTerm.push(term.substring(i, i+1));
                    position = i+1;
                    i++;
            }
        }
        subTerm.push(term.substring(position));
        calculateTermValue(subTerm);
    }

    /**
     * Description: Given a term, calculates the value of that term, only for * and / operation, if flipps the term to calculate correctly
     */

    private void calculateTermValue(DynamicStack<String> subTerm){
        DynamicStack<String> flippedSubTerm = new DynamicStack<String>();
        while (!subTerm.isEmpty()){
            flippedSubTerm.push(subTerm.peek());
            subTerm.pop();
        }

        double termValue = Double.parseDouble(flippedSubTerm.peek());
        flippedSubTerm.pop();
        while(!flippedSubTerm.isEmpty()) {
            if(flippedSubTerm.peek().charAt(0) == MULTIPLICATION_OPERATOR_ASCII){
                flippedSubTerm.pop();
                termValue = termValue * Double.parseDouble(flippedSubTerm.peek());
                flippedSubTerm.pop();
            }else if (flippedSubTerm.peek().charAt(0) == DIVISION_OPERATOR_ASCII){
                flippedSubTerm.pop();
                termValue = termValue / Double.parseDouble(flippedSubTerm.peek());
                flippedSubTerm.pop();
            }else{
                break;
            }
        }
        memory.push(String.valueOf(termValue));
    }
    /**
     * Description: Calculates the final result, using the memory stack
     */

    private void calculateResult(){
        result = Double.parseDouble(memory.peek());
        memory.pop();
        while(!memory.isEmpty()){
            if(memory.peek().charAt(0) == PLUS_OPERATOR_ASCII){
                memory.pop();
                result += Double.parseDouble(memory.peek());
                memory.pop();
            }else{
                memory.pop();
                result -= Double.parseDouble(memory.peek());
                memory.pop();
            }
        }
        System.out.println(operation + " = " + result);
        memory.push(String.valueOf(result));
        calculatorMain();
    }

    /**
     * Description: Principal main, asks the user what action to do and manages the calculator
     */

    public void calculatorMain(){
        System.out.println("\n 1.Calculate operation"+"\n 2.Clean memory"+"\n 3.Check out last memory number"+"\n 4.Exit program\n");
        System.out.println("Introduce a valid command:");
        Scanner command = new Scanner(System.in);
        int aCase =command.nextInt();
        switch (aCase){
            case 1:
                System.out.println("Introduce an operation: \n");
                if(!memory.isEmpty()){
                    System.out.println("Ans");
                }
                calculate();
                calculatorMain();
                break;
            case 2:
                memory.empty();
                System.out.println("Memory cleaned");
                calculatorMain();
                break;
            case 3:
                if(memory.isEmpty())
                    System.out.println("Memory is empty!");
                else
                    System.out.println(memory.peek());
                calculatorMain();
                break;
            case 0:
                System.exit(0);
                break;

            default:
                System.out.println("Please enter a valid command!");
                calculatorMain();
                break;

        }
    }

}
