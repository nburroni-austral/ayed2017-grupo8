package TP4Cola.Exercise1;

import TP3Pila.Ejercicio1.DynamicStack;
import TP4Cola.Exercise1.DynamicQueue;

/**
 * Created by Lucas on 6/4/2017.
 */

public class Palindrome {
    private static int ASCII_CODE_LETTER_A = 97;
    private static int ASCII_CODE_LETTER_Z = 122;

    private static DynamicStack<Character> stack = new DynamicStack<Character>();
    private static DynamicQueue<Character> queue = new DynamicQueue<Character>();

    /**
     * Description: checks if a given word or phrase is a palindrome
     * @param string: word or phrase to analyze
     * @return true if the word or phrase is a palindrome or false if it is not
     */

    public static boolean palindrome2(String string){
        stack.empty();
        queue.empty();
        fillQueueAndStack(string);
        for (int i = 0; i < string.length(); i++) {
            if(stack.peek() != queue.dequeue()){
                return false;
            }
            stack.pop();
        }
        return true;
    }

    /**
     * Description: auxiliary method that fills the stack and queue for evaluating the word or phrase, eliminating everything but
     * letters and converting them to lower case.
     */

    private static void fillQueueAndStack(String string){
        string = string.toLowerCase();
        for (int i = 0; i < stack.size(); i++) {
            if(string.charAt(i) >= ASCII_CODE_LETTER_A && string.charAt(i) <= ASCII_CODE_LETTER_Z){
                stack.push(string.charAt(i));
                queue.enqueue(string.charAt(i));
            }
        }
    }
}
