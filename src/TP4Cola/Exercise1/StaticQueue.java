package TP4Cola.Exercise1;

import struct.istruct.Queue;

/**
 * Created by santiagohazana on 4/5/17.
 */
public class StaticQueue<Q> implements Queue<Q> {

    int front;
    int back;
    int size;
    int capacity;
    Q[] queue;

    public StaticQueue(){
        this.front = 0;
        this.back = 0;
        this.size = 0;
        this.capacity = 10;
        queue = (Q[]) new Object[capacity];
    }

    public void enqueue(Q o) {
        size ++;
        if(size!=capacity && back<=(size-1)){
            queue[back] = o;
            back++;
        }else{
            if(back == capacity && size < capacity){
                back = 0;
                queue[back] = o;
                back++;
            }else{
                grow();
                back = size;
                queue[back] = o;
                back++;
            }
        }
    }

    public Q dequeue() {
        front++;
        return queue[front-1];
    }

    public boolean isEmpty() {
        return (size == 0);
    }

    public int length() {
        return capacity;
    }

    public int size() {
        return size;
    }

    public void empty() {
        queue = (Q[]) new Object[capacity];
        front = 0;
        back = 0;
        size = 0;
    }

    private void grow() {
        capacity *= 2;
        Q[] newQueue = (Q[]) new Object[capacity];
        int index = 0;

        while (!isEmpty()){
            newQueue[index] = dequeue();
        }
        queue = newQueue;
        
//        if(front < back){
//            for (int i = front; i < back; i++) {
//                newQueue[startingIndex++] = queue[i];
//            }
//        }else{
//            for (int i = front; i < capacity; i++) {
//                newQueue[startingIndex++] = queue[i];
//            }
//            for (int i = 0; i < back; i++) {
//                newQueue[startingIndex++] = queue[i];
//            }
//        }
    }
}
