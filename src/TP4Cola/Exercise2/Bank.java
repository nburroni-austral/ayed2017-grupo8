package TP4Cola.Exercise2;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by santiagohazana on 4/11/17.
 */
public class Bank {


    private ArrayList<Cashier> cashiers;
    private Strategy strategy;
    private int closeTime = 18000; //in seconds, equals to 5 hours
    private boolean open;

    public Bank() throws IOException {
        this.cashiers = new ArrayList<Cashier>();
        addCashier(new Cashier(30,90));
        addCashier(new Cashier(30,120));
        addCashier(new Cashier(30,150));
    }

    public ArrayList<Cashier> getCashiers() {
        return cashiers;
    }

    public void addCashier(Cashier cashier){
        cashiers.add(cashier);
    }


    private void generateReport() throws IOException {
            String content="";
            for(Registry registry:strategy.getRegistries()){
                content += registry.toString();
                File file = new File("ReportA.txt");
                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(content);
                bw.close();

        }
    }

    public void onClientEntered(Client client){
        strategy.setNewClient(client);
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) throws IOException {
        this.strategy = strategy;
        thread.start();
    }

    private void startDay() throws IOException {
        open = true;
        System.out.println("Day timer started");
        float startTime = System.currentTimeMillis();
        float elapsedTime = 0f;

        while (elapsedTime < closeTime*1000) {
            System.out.println("Trying to send client to cashier in bank");
            elapsedTime = (new Date()).getTime() - startTime;
            strategy.sendClientToCashier();
        }
        open = false;
        thread.interrupt();
        System.out.println("Bank closed...");
        attendClients();
    }

    private void attendClients() throws IOException {
        while (strategy.areQueuesEmpty()){
            strategy.sendClientToCashier();
        }
        generateReport();
    }

    public boolean isOpen(){
        return open;
    }

    Thread thread = new Thread(){
        public void run(){
            try {
                startDay();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

}
