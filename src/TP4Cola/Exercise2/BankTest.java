package TP4Cola.Exercise2;

import java.io.IOException;

/**
 * Created by santiagohazana on 4/12/17.
 */
public class BankTest {

    public static void main(String[] args) throws IOException {

        Bank bank = new Bank();
        GenerateClients generateClients = new GenerateClients(bank);
        StrategyA strategyA = new StrategyA(bank);
        StrategyB strategyB = new StrategyB(bank);
        bank.setStrategy(strategyA);
        generateClients.generateClients();

    }



}
