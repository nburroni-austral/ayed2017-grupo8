package TP4Cola.Exercise2;

import java.util.Date;
import java.util.Random;

/**
 * Created by santiagohazana on 4/11/17.
 */
public class Cashier {

    private boolean free;
    private int minTime;
    private int maxTime;

    public Cashier(int minTime, int maxTime) {
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.free=true;
    }

    public boolean isFree() {
        return free;
    }

    public int startTimer(){
        System.out.println("Cashier Timer started");
        free = false;
        Random random = new Random();
        int time = random.nextInt(maxTime-minTime)+minTime;

        float startTime = System.currentTimeMillis();
        float elapsedTime = 0f;

        while (elapsedTime < time*1000) {
            elapsedTime = (new Date()).getTime() - startTime;
        }

        free = true;
        thread.interrupt();
        return time;
    }

    public Thread thread = new Thread(){
        public void run() {
            startTimer();
        }
    };


}
