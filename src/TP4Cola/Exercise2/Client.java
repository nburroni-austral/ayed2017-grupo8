package TP4Cola.Exercise2;

import java.util.Random;

/**
 * Created by santiagohazana on 4/11/17.
 */
public class Client {

    private String name;
    private String surname;

    public Client(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public boolean decideToStay(int size){
        Random random = new Random();
        if(size<=3){
            return true;
        }else if (size>3 && size<9){
            int result = random.nextInt(4)+1;
            if(result==1 || result==4)
                return true;
        }else if (size>=9){
            int result = random.nextInt(1);
            if(result==1) {
                System.out.println("Enter bank");
                return true;
            }
        }
        System.out.println("To many people");
        return false;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
