package TP4Cola.Exercise2;

import java.util.Date;
import java.util.Random;

/**
 * Created by santiagohazana on 4/12/17.
 */
public class GenerateClients {

    Bank bank;


    public GenerateClients(Bank bank){
        this.bank = bank;
    }

    public void generateClients() {
        Random rn = new Random();
        int numberOfClients = rn.nextInt(6)+1;
        int enterTime = 90;
        float startTime = System.currentTimeMillis();
        float elapsedTime = 0f;

        while (elapsedTime < enterTime*1000) {
            elapsedTime = (new Date()).getTime() - startTime;
        }
        for (int i = 0; i < numberOfClients; i++) {
            if(bank.isOpen()) {
                System.out.println("Client generated");
                bank.onClientEntered(new Client("Santiago", "Hazaña"));
            }
            else
                System.out.println("Bank closed!");
            thread.interrupt();
                break;
        }
        generateClients();
    }

    Thread thread = new Thread(){
        public void run(){
            generateClients();
        }

    };
}
