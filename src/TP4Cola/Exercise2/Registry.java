package TP4Cola.Exercise2;

/**
 * Created by Lucas on 12/4/2017.
 */
public class Registry {
    Client client;
    int timeDuration;

    public Registry(Client client, int timeDuration) {
        this.client = client;
        this.timeDuration = timeDuration;

    }

    public Client getClient() {
        return client;
    }

    public int getTimeDuration() {
        return timeDuration;
    }
}
