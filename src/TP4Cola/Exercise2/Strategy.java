package TP4Cola.Exercise2;

import java.util.ArrayList;

/**
 * Created by santiagohazana on 4/11/17.
 */
public interface Strategy {

    void sendClientToCashier();

    void clientEnteredBank();

    void sendClientToQueue();

    void setNewClient(Client client);

    int getCurrentClients();

    boolean areQueuesEmpty();

    ArrayList<Registry> getRegistries();

}
