package TP4Cola.Exercise2;

import TP4Cola.Exercise1.DynamicQueue;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by santiagohazana on 4/11/17.
 */
public class StrategyA implements Strategy{


    protected Bank bank;
    protected DynamicQueue<Client> clients;
    protected Client newClient;
    protected Random rn;
    protected ArrayList<Registry> registries;


    public StrategyA(Bank bank){
        this.bank = bank;
        this.clients=new DynamicQueue<Client>();
        this.rn=new Random();
        this.registries=new ArrayList<Registry>();
    }

    public void sendClientToCashier() {
        if(!clients.isEmpty()) {
            int select = rn.nextInt(bank.getCashiers().size());
            if (bank.getCashiers().get(select).isFree()) {
                Client auxClient = clients.dequeue();
//                registries.add(new Registry(auxClient,bank.getCashiers().get(select).startTimer()));
                bank.getCashiers().get(select).thread.start();
            } else {
                sendClientToCashier();
            }
        }
    }

    public void clientEnteredBank() {
        if(newClient.decideToStay(getCurrentClients())){
            sendClientToQueue();
        }
    }

    public void sendClientToQueue() {
        clients.enqueue(newClient);
    }

    public void setNewClient(Client client) {
        newClient = client;
    }

    public int getCurrentClients() {
        return clients.size();
    }

    public boolean areQueuesEmpty() {
        if(clients.isEmpty()) return true;
        return false;
    }

    public ArrayList<Registry> getRegistries() {
        return registries;
    }

}
