package TP4Cola.Exercise2;

import TP4Cola.Exercise1.DynamicQueue;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by santiagohazana on 4/11/17.
 */
public class StrategyB implements Strategy{


    protected Bank bank;
    protected Client newClient;
    protected Random rn = new Random();
    protected ArrayList<DynamicQueue<Client>> queues;
    protected ArrayList<Registry> registries;

    public StrategyB(Bank bank){
        this.bank = bank;
        queues = new ArrayList<DynamicQueue<Client>>();
        queues.add(new DynamicQueue<Client>());
        queues.add(new DynamicQueue<Client>());
        queues.add(new DynamicQueue<Client>());
        this.registries=new ArrayList<Registry>();
    }

    public void sendClientToCashier() {
        if(!queues.get(0).isEmpty() && !queues.get(1).isEmpty() && !queues.get(2).isEmpty()) {
            int select = rn.nextInt(bank.getCashiers().size());
            if (bank.getCashiers().get(select).isFree()) {
                Client auxClient = queues.get(select).dequeue();
//                registries.add(new Registry(auxClient,bank.getCashiers().get(select).startTimer()));
                bank.getCashiers().get(select).thread.start();
            } else {
                sendClientToCashier();
            }
        }
    }

    public void clientEnteredBank() {
        if(newClient.decideToStay(getCurrentClients())){
            sendClientToQueue();
        }
    }

    public void sendClientToQueue() {
        int clients = getCurrentClients();
        for (DynamicQueue<Client> queue: queues) {
            if(queue.size() == clients)
                queue.enqueue(newClient);
        }

    }

    public void setNewClient(Client client) {
        newClient=client;
    }

    public int getCurrentClients() {
       int result = queues.get(0).size();
        for (DynamicQueue<Client> queue: queues) {
            if(queue.size() < result)
                result = queue.size();
        }
        return result;
    }

    public boolean areQueuesEmpty() {
        if(queues.get(0).isEmpty() && queues.get(1).isEmpty() && queues.get(2).isEmpty()) return true;
        return false;
    }

    public ArrayList<Registry> getRegistries() {
        return registries;
    }

}
