package TP4Cola.Exercise3;

import TP4Cola.Exercise1.DynamicQueue;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Lucas on 12/4/2017.
 */
public class QueuePriority <T>{
    DynamicQueue<T> topQueue;
    DynamicQueue<T> middleQueue;
    DynamicQueue<T> lowQueue;
    ArrayList<DynamicQueue<T>> priorityQuees;

    /**
     * Construtor which created a list with three initials queues empties.
     */
    public QueuePriority() {
        topQueue= new DynamicQueue<T>();
        middleQueue= new DynamicQueue<T>();
        lowQueue = new DynamicQueue<T>();
        this.priorityQuees= new ArrayList<DynamicQueue<T>>();
        priorityQuees.add(topQueue);
        priorityQuees.add(middleQueue);
        priorityQuees.add(lowQueue);
    }

    /**
     * Enqueue a generic element in a specific level.
     * If  level doesn´t exist generate a new queue which is the last priority and enqueue that element in that queue.
     */
    public void enqueue( T elementToEnqueue ,int priorityLevel){
        if(priorityLevel<priorityQuees.size())
        priorityQuees.get(priorityLevel).enqueue(elementToEnqueue);
        else{
            System.out.println("No se encontro prioridad, se asigna su elemento a su nueva prioridad en: "+priorityQuees.size()+1);
            priorityQuees.add(new DynamicQueue<T>());
            priorityQuees.get(priorityQuees.size()+1).enqueue(elementToEnqueue);

        }
    }

    /**
     * Dequeue the element with the first and max priority possible.
     */
    public Object dequeue() throws NullPointerException{

        int count=0;
        if (count>priorityQuees.size())
            System.out.println("There are not queue available to  dequeue.");

        while (priorityQuees.get(count).isEmpty())
                count++;

        return priorityQuees.get(count).dequeue();

    }

    /**
     * Ask by console a priority level.
     */
    public int askForPriorityLevel(){
            System.out.println("Inserte un nivel de prioridad del 1 en adelante,"+"\n"+"siendo 1 la maxima prioridad deciada. ");
        Scanner scanner = new Scanner(System.in);
        int command = scanner.nextInt();
        if(command<1)
            return askForPriorityLevel();
        return command;
    }
}
