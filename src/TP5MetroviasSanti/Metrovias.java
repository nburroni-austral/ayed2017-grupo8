package TP5MetroviasSanti;

import List.DynamicList;

import java.util.Random;

/**
 * Created by santiagohazana on 4/19/17.
 */
public class Metrovias {
    private Random rn = new Random();

    private int cycleCount = 0;
    private final int closeCycle = 57600;
    private int almostCloseTimeCycle = closeCycle - 30;

    DynamicList<Window> windows;

    /**
     * Description: generates a new simulation with the given windows. Starts a new day.
     * @param windowCount = takes the number of windows to operate with, between 3 and 10
     */

    public Metrovias(int windowCount){
        if(windowCount > 2 && windowCount < 11) {
            windows = new DynamicList<Window>();
            for (int i = 0; i < windowCount; i++) {
                windows.insertNext(new Window());
            }
            startDay();
        }else{
            System.out.println("Invalid number of windows");
        }
    }

    /**
     * Description: the timer of the simulation, keeps the count of the cycles and attends the clients each cycle.
     * Within the last 30 minutes, it attends all the remaining clients.
     */

    private void startDay() {
        for (int i = 0; i < almostCloseTimeCycle; i+=10) {
            generateClientsAndEnqueue();
            attend();
            cycleCount++;
        }
        attendRemaining();
        generateReport();
    }

    /**
     * Description: generates 5 clients each cycle and sends them to random windows
     */

    private void generateClientsAndEnqueue() {
        for (int i = 0; i < 5; i++) {
            int random = rn.nextInt(windows.size());
            windows.goTo(random);
            windows.getActual().addClientToQueue(new Client(), cycleCount);
        }
    }

    /**
     * Description: calls the attend method of all the windows
     */

    private void attend() {
        for (int i = 0; i < windows.size(); i++) {
            windows.goTo(i);
            windows.getActual().attend(cycleCount);
        }
    }

    /**
     * Description: calls the attendAll of all windows method to attend all the remaining clients
     */

    private void attendRemaining() {
        for (int i = 0; i < windows.size(); i++) {
            windows.goTo(i);
            windows.getActual().attendAll(cycleCount);
        }
    }

    /**
     * Description: generates a report at the end of the day, giving information of each window
     */

    private void generateReport() {
        double totalCash = 0;
        int totalClients = 0;
        System.out.println("Day closed: \n");
        for (int i = 0; i < windows.size(); i++) {
            windows.goTo(i);
            totalCash += windows.getActual().getCash();
            totalClients += windows.getActual().getClientCount();
            System.out.println("----------------------------------------");
            System.out.println("Window " + (i+1));
            System.out.println("Average wait time: " + (int)windows.getActual().getClientAverageWaiteTime() + "s");
            System.out.println("Income: " + "$" + (float)windows.getActual().getCash());
            System.out.println("Idle time: " + (int)windows.getActual().getIdleTime() + "s");
            System.out.println("Clients attended: " + windows.getActual().getClientCount());
        }
        System.out.println("----------------------------------------");
        System.out.println("Total cash: " + "$" + (float)totalCash);
        System.out.println("Total clients: " + totalClients);
    }

}
