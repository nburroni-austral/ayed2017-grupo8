package TP5MetroviasSanti;

import TP4Cola.Exercise1.DynamicQueue;

/**
 * Created by santiagohazana on 4/19/17.
 */
public class Window {
    private DynamicQueue<Client> clientsQueue;
    private int clientCount = 0;
    private float clientCycleCount = 0;
    private double cash = 0;
    private float idleTime = 0;
    private static float ticketValue = 0.7f;

    public Window(){
        clientsQueue = new DynamicQueue<Client>();
    }

    /**
     * Description: adds a client given to the window queue
     * @param c: client to enqueue
     * @param cycle: the cycle in which the client entered the simulation
     */

    public void addClientToQueue(Client c, int cycle){
        c.setEnterCycle(cycle);
        clientsQueue.enqueue(c);
    }

    /**
     * Description: attends the next client if the random possibility is < 0.3, given that case id attends the client,
     * summing the ticket value and getting the time the client waited. If no client is in the queue, it checks that time as idle
     * @param actualCycle: the cycle the simulation is at, to calculate the time the client spent in the queue
     */

    public void attend(int actualCycle){
        if(clientsQueue.isEmpty()){
            idleTime += 10;
        }else {
            if (Math.random() < 0.3) {
                cash += ticketValue;
                clientCount++;
                clientCycleCount += (actualCycle - clientsQueue.dequeue().getEnterCycle());
            }
        }
    }

    /**
     * Description: attends all the remaining clients in the queue
     * @param actualCycle: the cycle the simulation is at, to calculate the time the client spent in the queue
     */

    public void attendAll(int actualCycle){
        while(!clientsQueue.isEmpty()){
            cash += ticketValue;
            clientCount++;
            clientCycleCount += (actualCycle - clientsQueue.dequeue().getEnterCycle());
        }
    }

    public float getIdleTime() {
        return idleTime;
    }

    public double getCash() {
        return cash;
    }

    public float getClientAverageWaiteTime() {
        return clientCycleCount * 10 / clientCount;
    }

    public int getClientCount() {
        return clientCount;
    }
}
