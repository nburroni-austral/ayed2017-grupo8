package TP6ListaOrdenada;

import java.io.Serializable;

/**
 * Created by Lucas on 10/5/2017.
 */
public class Bus implements Comparable<Bus>, Serializable{
    private int quantitySeats;
    private int line;
    private int internalID;
    private boolean allowImpairment;


    public Bus(int line, int internalID, int quantitySeats, boolean impairmentAvailable) {
        this.quantitySeats = quantitySeats;
        this.line = line;
        this.internalID = internalID;
        this.allowImpairment = impairmentAvailable;
    }

    public int getQuantitySeats() {
        return quantitySeats;
    }

    public int getLine() {
        return line;
    }

    public int getInternalID() {
        return internalID;
    }

    public boolean isAllowImpairment() {
        return allowImpairment;
    }

    @Override
    public int compareTo(Bus busCompare) {
        if(line <busCompare.getLine())
            return -1;
        else if (line == busCompare.getLine()){
            if(internalID<busCompare.getInternalID())
                return -1;
            else if (line == busCompare.getInternalID())
                return 0;
            else
                return 1;
        }
        return 1;
    }
}
