package TP6ListaOrdenada;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Lucas on 10/5/2017.
 */
public class BusSystem implements Serializable{
    private DynamicListSorted<Bus> buses;
    private Scanner scanner;

    public BusSystem() {
        this.buses = new DynamicListSorted<Bus>();
        this.scanner= new Scanner(System.in);
    }

    public void runSystem() throws IOException, ClassNotFoundException {
            System.out.println("1) Add bus.\n"+
                    "2) Remove bus.\n"+
                    "3) Get report of buses.\n"+
                    "4) Get report of which buses are Impairment capable per bus line.\n"+
                    "5) Get how many buses per line have more than 27 seats.\n"+
                    "6) Save bus list.\n"+
                    "7) Load bus list.\n"+
                    "0) Exit\n");
        doAction(scanner.nextInt());
        runSystem();
    }

    private void doAction(int command) throws IOException, ClassNotFoundException {
        switch (command){
            case 1:
                System.out.println("Insert bus Line, internal ID, Seats, Impairment Capable:");
                addBus(new Bus(scanner.nextInt(),scanner.nextInt(),scanner.nextInt(),scanner.nextBoolean()));
                break;
            case 2:
                System.out.println("Insert bus Line, internal ID:");
                removeBus(scanner.nextInt(),scanner.nextInt());
                break;
            case 3:
                getBusLines();
                break;
            case 4:
                numberOfBusesImpairmentCapableByLine();
                break;
            case 5:
                minOfSeatsRequireByLine(27);
                break;
            case 6:
                saveBusesListFile();
                break;
            case 7:
                loadBusesListFile();
                break;
            case 0:
                System.exit(0);
            default:
                System.out.println("Invalid command");
                break;

        }
    }

    public void addBus(Bus bus){
        buses.insert(bus);
    }

    public void removeBus (int insertBusID, int insertBusInternalID){
        for (int index=0; index<buses.size(); index++){
            buses.goTo(index);
            if(buses.getActual().getLine()==insertBusID && buses.getActual().getInternalID()==insertBusInternalID){
                buses.remove();
                return;
            }
        }
        System.out.println("Bus not found!");
    }

    public void numberOfBusesImpairmentCapableByLine() {
        if (!buses.isVoid()) {
            System.out.println("Numbers Of Buses which allows impairment: ");
            int count = 0;
            int index = 0;
            int busLineAux = 0;

            while (index < buses.size()) {

                buses.goTo(index);

                if (buses.getActualPosition() == 0 || busLineAux != buses.getActual().getLine()) {
                    busLineAux = buses.getActual().getLine();
                    for (int i = 0; i < buses.size(); i++) {
                        buses.goTo(i);
                        if (busLineAux == buses.getActual().getLine() && buses.getActual().isAllowImpairment())
                            count++;
                    }
                    System.out.println("Bus Line: " + busLineAux + " quantity: " + count);
                    count = 0;
                }
                index++;
            }
        }
    }

    public void minOfSeatsRequireByLine(int numberOfSeats) {
        if (!buses.isVoid()) {
            System.out.println("Numbers Of Buses which allows " + numberOfSeats + " seats or more:");
            int count = 0;
            int index = 0;
            int busLineAux = 0;

            while (index < buses.size()) {

                buses.goTo(index);

                if (buses.getActualPosition() == 0 || busLineAux != buses.getActual().getLine()) {
                    busLineAux = buses.getActual().getLine();
                    for (int i = 0; i < buses.size(); i++) {
                        buses.goTo(i);
                        if (busLineAux == buses.getActual().getLine() && buses.getActual().getQuantitySeats() >= numberOfSeats)
                            count++;
                    }
                    System.out.println("Bus ID: " + busLineAux + " quantity: " + count);
                    count = 0;
                }
                index++;
            }
        }
        else
            System.out.println("No buses loaded!");

    }

    public void getBusLines(){
        for (int index=0; index<buses.size();index++){
            buses.goTo(index);
            Bus aux = buses.getActual();
            System.out.println("Bus Line: "+aux.getLine()+"\t"+"Bus Internal ID: "+aux.getInternalID()+"\t"+
                    "Seats: "+aux.getQuantitySeats()+"\t"+"Impairment Available: "+aux.isAllowImpairment());
        }
    }

    public void saveBusesListFile() throws IOException {
        if(!buses.isVoid()) {
            FileOutputStream out = new FileOutputStream("Bus list");
            ObjectOutputStream serializeObject = new ObjectOutputStream(out);
            serializeObject.writeObject(buses);
            serializeObject.close();
            System.out.println("File saved");
        }
        else {
            System.out.println("No buses load!");
        }
    }

    public void loadBusesListFile() throws IOException, ClassNotFoundException{
        try {
            FileInputStream out = new FileInputStream("Bus list");
            ObjectInputStream serializeObject = new ObjectInputStream(out);
            //noinspection unchecked
            buses = (DynamicListSorted<Bus>) serializeObject.readObject();
            serializeObject.close();
            System.out.println("File loaded");
        }catch (ClassNotFoundException e){
            System.out.println("File not found!");
        }
    }
}
