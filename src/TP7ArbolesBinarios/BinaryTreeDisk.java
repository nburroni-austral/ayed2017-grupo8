package TP7ArbolesBinarios;

import java.io.*;

/**
 * Created by Lucas on 11/4/2017.
 */
public class BinaryTreeDisk{

    public static <T> void record(BinaryTree<T> tree) throws IOException{
        FileOutputStream salida = new FileOutputStream("Data");
        ObjectOutputStream objSeriado = new ObjectOutputStream(salida);
        objSeriado.writeObject(tree);
        objSeriado.close();
    }

    public static <T> BinaryTree<T> recover() throws IOException, ClassNotFoundException {
        FileInputStream salida = new FileInputStream("Data");
        ObjectInputStream objSeriado = new ObjectInputStream(salida);
        BinaryTree<T> tree = (BinaryTree) objSeriado.readObject();
        objSeriado.close();

        return tree;
    }


}
