package TP7ArbolesBinarios;

/**
 * Created by Lucas on 9/4/2017.
 */

public class Ejercicio13 {

    public static int weight(BinaryTree a){
        if(a.isEmpty())
            return 0;
        else
            return 1+weight(a.getLeft())+weight(a.getRight());
    }

    public static int quantityLeaves(BinaryTree a){
        if(a.isEmpty())
            return 0;
        if(a.getLeft().isEmpty() && a.getRight().isEmpty())
            return 1;
        return quantityLeaves(a.getLeft())+ quantityLeaves(a.getRight());
    }

    public static <T> int quantityElement(BinaryTree a, T element){
        int result=0;
        if(!a.isEmpty()) {
            if (a.getRoot()==element)
            result++;
            result=result+quantityElement(a.getLeft(),element) + quantityElement(a.getRight(),element);
        }
        return result;

    }

    public static int quantityOfElementsInLevel(BinaryTree a, int requireLevel) {
        int currentLevel=0;
        int result=0;
        if (!a.isEmpty() && currentLevel==requireLevel)
            result++;
        else if(!a.isEmpty() && currentLevel<requireLevel){
            currentLevel++;
            result= quantityOfElementsInLevel(a.getLeft(),requireLevel,currentLevel)+quantityOfElementsInLevel(a.getRight(),requireLevel,currentLevel);
        }
        return result;
    }

    private static int quantityOfElementsInLevel(BinaryTree a, int requireLevel, int currentLevel) {
        int result = 0;
        if (!a.isEmpty() && currentLevel == requireLevel)
            result++;
        else if (!a.isEmpty() && currentLevel < requireLevel) {
            currentLevel++;
            result = quantityOfElementsInLevel(a.getLeft(), requireLevel, currentLevel) + quantityOfElementsInLevel(a.getRight(), requireLevel, currentLevel);
        }
        return result;
    }

    public static int height(BinaryTree a){
        int levelCount = 0;
        if(!a.getLeft().isEmpty() && !a.getRight().isEmpty()){
            levelCount += (1 + Math.max(height(a.getLeft()), height(a.getRight())));
        } else if (!a.getRight().isEmpty()){
            levelCount += (1 + height(a.getRight()));
        } else if (!a.getLeft().isEmpty()){
            levelCount += (1 + height(a.getLeft()));
        }

        return levelCount;
    }

}
