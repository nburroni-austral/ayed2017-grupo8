package TP7ArbolesBinarios;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Lucas on 9/4/2017.
 */

public class Ejercicio14 {

    public static int additionIntBinaryTree(BinaryTree<Integer> a){
        int result=0;
        if(!a.isEmpty()) {
            result = result + a.getRoot();
            result = result +additionIntBinaryTree(a.getLeft()) + additionIntBinaryTree(a.getRight());
        }
        return result;
    }

    public static int additionIntMultpleThreeBinaryTree(BinaryTree<Integer> a) {
        int result=0;
        if(!a.isEmpty()) {
            int aux = a.getRoot();
            if(aux%3==0) {
                result = result + a.getRoot();
            }
            result= result +additionIntMultpleThreeBinaryTree(a.getLeft())+additionIntMultpleThreeBinaryTree(a.getRight());
        }
        return result;
    }

    public static <T> boolean equals(BinaryTree<T> a , BinaryTree<T> b){
        boolean result = true;
        if(!a.isEmpty() && !b.isEmpty()){
            if (!a.getRoot().equals(b.getRoot()))
                return false;
            result=equals(a.getLeft(),b.getLeft()) && equals(a.getRight(),b.getRight());
        }
        return result;

    }

    public static <T> boolean isomorfos(BinaryTree<T> a, BinaryTree<T> b){
        if(!(a.isEmpty()==b.isEmpty()))
            return false;
        if(!a.isEmpty()) {
            if (!(a.getLeft().isEmpty() == b.getLeft().isEmpty()))
                return false;
            if (!(a.getRight().isEmpty() == b.getRight().isEmpty()))
                return false;
            return isomorfos(a.getRight(), b.getRight()) && isomorfos(a.getLeft(), a.getLeft());
        }
        return true;
    }

    public static <T> boolean semejantes(BinaryTree<T> a, BinaryTree<T> b){
        ArrayList arrayA = new ArrayList();
        ArrayList arrayB = new ArrayList();

        fillArray(arrayA,a);
        fillArray(arrayB,b);

        Collections.sort(arrayA);
        Collections.sort(arrayB);

        for (Object element:
             arrayB) {
            if(!arrayA.contains(element))
                return false;
            arrayA.remove(element);

        }

        if(!arrayA.isEmpty()){
            return false;
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    private static <T> void fillArray(ArrayList arrayList, BinaryTree<T> tree){
        if(!tree.isEmpty()){
            arrayList.add(tree.getRoot());
            fillArray(arrayList,tree.getLeft());
            fillArray(arrayList,tree.getRight());
        }
    }

    public static boolean complete ( BinaryTree a ){

        if(!a.isEmpty()) {
            if ((a.getLeft().isEmpty() && !a.getRight().isEmpty()))
                return false;
            else if(!a.getLeft().isEmpty() && a.getRight().isEmpty())
                return false;
            return complete(a.getLeft()) && complete(a.getRight());
        }
        return true;
    }

    public static boolean full ( BinaryTree a ){
        if(!complete(a))
            return false;
        int leafesLevel = Ejercicio13.height(a);
        ArrayList leafes = frontera(a, 0);
        for (Object elem : leafes) {
            if(searchForLevel(a, elem, 0) != leafesLevel){
                return false;
            }
        }
        return true;
        /* Informa si un árbol binario está lleno */
    }

    public static boolean estable( BinaryTree<Integer> a ){

        if(a.isEmpty() || (!a.getRight().isEmpty() || !a.getLeft().isEmpty()) )
            return false;
        return true;

        /* Un árbol de valores enteros es estable si es vacío, consta de un único elemento
         para todo elemento de la estructura su padre es mayor. */
    }

    public static <T> boolean ocurrenceBinaryTree( BinaryTree<T> a, BinaryTree<T> b ) {
        if(!a.isEmpty()){
            if(!b.isEmpty()){
                if(equals(a,b))
                    return true;
                else if (ocurrenceBinaryTree(a.getRight(),b))
                    return true;
                else if (ocurrenceBinaryTree(a.getLeft(),b))
                    return true;
            }
        }
        return false;
    }

    public static void mostrarFrontera(BinaryTree a) {
        ArrayList frontera = frontera(a, 0);

        for (Object element: frontera) {
            System.out.println(element.toString());
        }
    }

    public static ArrayList frontera( BinaryTree a, Object elem ){
        ArrayList frontera = new ArrayList();
        if(!a.isEmpty()){
            if(a.getLeft().isEmpty() && a.getRight().isEmpty())
                frontera.add(a.getRoot());
            else if(a.getRight().isEmpty())
               frontera.addAll(frontera(a.getLeft(), elem));
            else if (a.getLeft().isEmpty())
                frontera.addAll(frontera(a.getRight(), elem));
            else {
                frontera.addAll(frontera(a.getRight(), elem));
                frontera.addAll(frontera(a.getLeft(), elem));
            }
        }
        /* Se define frontera de un árbol binario, a el conjunto formado por los elementos
        almacenados en las hojas.
         */
        return frontera;
    }

    private static int searchForLevel(BinaryTree a, Object elem, int level){
        if(a.isEmpty())
            return 0;
        if(a.getRoot().equals(elem))
            return level;

        int result=searchForLevel(a.getLeft(),elem,level+1);

        if(result!=0)
            return result;

        result = searchForLevel(a.getRight(),elem,level+1);
        return result;
    }


}
