package TP7ArbolesBinarios;

import java.util.ArrayList;

/**
 * Created by Lucas on 10/4/2017.
 */
@SuppressWarnings("ALL")
public class ShowBinaryTree{

    private static ArrayList arrayList = new ArrayList();


    public static <T> ArrayList byPreorder(BinaryTree<T> tree){
        preorderAlgorithm(tree);
        ArrayList array = new ArrayList();
        fillArrayListAnswers(array);
        return array;
    }

    private static <T> void preorderAlgorithm(BinaryTree<T> tree) {
        if(!tree.isEmpty()){
            arrayList.add(tree.getRoot());
            if(!tree.getLeft().isEmpty())
                preorderAlgorithm(tree.getLeft());
            if(!tree.getRight().isEmpty())
                preorderAlgorithm(tree.getRight());
        }
    }


    public static <T> ArrayList byInorder(BinaryTree<T> tree){
        inorderAlgorithm(tree);
        ArrayList array = new ArrayList();
        fillArrayListAnswers(array);
        return array;
    }

    private static <T> void inorderAlgorithm(BinaryTree<T> tree){
        if(!tree.isEmpty()){
            if(!tree.getLeft().isEmpty())
                inorderAlgorithm(tree.getLeft());
            arrayList.add(tree.getRoot());
            if(!tree.getRight().isEmpty())
                inorderAlgorithm(tree.getRight());
        }
    }


    public static <T> ArrayList byPostorder(BinaryTree<T> tree){
        postorderAlgorithm(tree);
        ArrayList array = new ArrayList();
        fillArrayListAnswers(array);
        return array;
    }

    private static <T> void postorderAlgorithm(BinaryTree<T> tree) {
        if(!tree.isEmpty()){
            if(!tree.getLeft().isEmpty())
                postorderAlgorithm(tree.getLeft());
            if(!tree.getRight().isEmpty())
                postorderAlgorithm(tree.getRight());
            arrayList.add(tree.getRoot());
        }
    }


    public static <T> ArrayList byLevel(BinaryTree<T> tree){
        levelAlgorithmInicial(tree);
        ArrayList array = new ArrayList();
        fillArrayListAnswers(array);
        return array;
    }

    private static <T> void levelAlgorithmInicial(BinaryTree<T> tree) {
        boolean left=false;
        boolean rigth=false;
        if(!tree.isEmpty()) {
            arrayList.add(tree.getRoot());
            if (!tree.getLeft().isEmpty()) {
                arrayList.add(tree.getLeft().getRoot());
                left=true;
            }
            if (!tree.getRight().isEmpty()) {
                arrayList.add(tree.getRight().getRoot());
                rigth=true;
            }

        }
        if(left)
            levelAlgorithm(tree.getLeft());
        if (rigth)
            levelAlgorithm(tree.getRight());

    }

    private static <T> void levelAlgorithm(BinaryTree<T> tree){
        boolean left=false;
        boolean rigth=false;
        if(!tree.isEmpty()) {
            if (!tree.getLeft().isEmpty()) {
                arrayList.add(tree.getLeft().getRoot());
                left=true;
            }
            if (!tree.getRight().isEmpty()) {
                arrayList.add(tree.getRight().getRoot());
                rigth=true;
            }

        }
        if(left)
            levelAlgorithm(tree.getLeft());
        if (rigth)
            levelAlgorithm(tree.getRight());
    }


    private static void emptyArrayList() {
        arrayList.removeAll(arrayList);
    }

    private static void fillArrayListAnswers(ArrayList array) {
        for (int i = 0; i<arrayList.size();i++){
            array.add(arrayList.get(i));
        }
        emptyArrayList();
    }
}
