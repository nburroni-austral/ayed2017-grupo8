package TP8ArbolesBusqueda;

import java.util.Scanner;

/**
 * Created by Lucas on 21/4/2017.
 */
public class Lamp implements Comparable<Lamp>{
    private String codeLamp;
    private String typeLamp;
    private int watts;
    private int quantity;
    private Scanner scanner;

    public Lamp() {
        this.scanner = new Scanner(System.in);
        setCodeLamp();
        setTypeLamp();
        setWatts();
        setQuantity();
    }

    public Lamp(String codeLamp, String typeLamp, int watts, int quantity) {
        this.codeLamp = codeLamp.toUpperCase();
        this.typeLamp = typeLamp.toUpperCase();
        this.watts = watts;
        this.quantity = quantity;
    }

    public void setWatts(){
        System.out.println("Set watts:");
        this.watts= scanner.nextInt();
    }

    public void setQuantity(){
        System.out.println("Set quantity of Lamp:");
        this.quantity= scanner.nextInt();
    }

    public void setTypeLamp(){
        System.out.println("Set your type of Lamp:");
        String aux = scanner.nextLine();
        aux.toUpperCase();
        if(aux.length()<=5){
            typeLamp=aux;
            return;
        }
        System.out.println("Your type should have a length from 10 or less."+"\n"+"Your actual length is: "+aux.length());
        setTypeLamp();
    }

    private void setCodeLamp(){
        System.out.println("Set your code of Lamp:");
        String aux = scanner.nextLine();
        aux.toUpperCase();
        if(aux.length()<=5){
            codeLamp=aux;
            return;
        }
        System.out.println("Your code should have a length from 5 or less."+"\n"+"Your actual length is: "+aux.length());
        setCodeLamp();
    }

    public String getCodeLamp() {
        return codeLamp;
    }

    public String getTypeLamp() {
        return typeLamp;
    }

    public int getWatts() {
        return watts;
    }

    public int getQuantity() {
        return quantity;
    }

    public void reduceQuantity(){
        quantity--;
    }

    public void raiseQuantity(int number){
        quantity += number;
    }

    @Override
    public int compareTo(Lamp l) {
        if(codeLamp.length()>l.getCodeLamp().length())
            return 1;
        else if(codeLamp.length()<l.getCodeLamp().length())
            return -1;
        return 0;
    }
}
