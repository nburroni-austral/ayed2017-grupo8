package TP8ArbolesBusqueda;

/**
 * Created by Lucas on 21/4/2017.
 */
public class Stock {
    private BinaryTreeSearch<Lamp> stock;

    public Stock() {
        this.stock = new BinaryTreeSearch<Lamp>();
    }

    public void addLamp(Lamp lamp){

        if(lookForLamp(lamp.getCodeLamp()) !=null) {
            lookForLamp(lamp.getCodeLamp()).raiseQuantity(1);
            showLampReport(lookForLamp(lamp.getCodeLamp()));
        }
        else{
            stock.insert(lamp);
            showLampReport(lamp);
        }
    }

    public Lamp lookForLamp(String codeLamp){
        Lamp aux = new Lamp(codeLamp,"aux",0,0);
        Lamp result;
        try {
            result=(Lamp) stock.find(aux);
        }
        catch (NullPointerException e){
            result=null;
        }

        return result;
    }

    private void removeLamp(Lamp lamp){
        if(lamp.getQuantity()==0){
            cleanLampFromStock(lamp.getCodeLamp());
            return;
        }
        lookForLamp(lamp.getCodeLamp()).reduceQuantity();
        showLampReport(lookForLamp(lamp.getCodeLamp()));

    }

    public Lamp getLamp (String codeLamp){
        Lamp result = lookForLamp(codeLamp);
        removeLamp(result);

        return result;
    }

    public void changeWatt(String codeLamp){
        Lamp lamp = lookForLamp(codeLamp);
        if(!stock.isEmpty()){
            if(lamp!=null)
                lamp.setWatts();
        }
    }

    public void changeQuantity(String codeLamp){
        Lamp lamp = lookForLamp(codeLamp);
        if(!stock.isEmpty()){
            if(lamp!=null)
                lamp.setQuantity();
        }
    }

    public void changeType(String codeLamp){
        Lamp lamp = lookForLamp(codeLamp);
        if(!stock.isEmpty()){
            if(lamp!=null)
                lamp.setTypeLamp();
        }
    }

    public void cleanLampFromStock(String codeLamp){
        Lamp aux = new Lamp(codeLamp,"aux",0,0);
        stock.remove(aux);
    }

    private void showLampReport(Lamp lamp){
        System.out.println("-------------------------------------");
        System.out.println("Code: "+lamp.getCodeLamp()+"\n"+"Type: "+lamp.getTypeLamp()
                +"\n"+"Watts: "+lamp.getWatts()+"\n"+"Quantity: "+lamp.getQuantity());
    }

    public BinaryTreeSearch<Lamp> getStock() {
        return stock;
    }
}
