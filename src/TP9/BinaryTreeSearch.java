package TP9;


import java.lang.Comparable;

/**
 * Created by Lucas on 19/4/2017.
 */
public class BinaryTreeSearch<T> {
    // Implementacion de un arbol binario de busqueda no balanceado
    // Autor Alicia Gioia

    private DoubleNode root;

    public BinaryTreeSearch(){
            root = null;
        }
    // METODOS COMUNES CON ARBOL BINARIO
    // precondicion: -
    public boolean isEmpty(){
            return (root == null);
        }

    // precondición: árbol distino de vacío
    public Object getRoot(){
            return root.elem;
        }

    // precondición: árbol distino de vacío
    public BinaryTreeSearch getLeft(){
        BinaryTreeSearch t = new BinaryTreeSearch();
        t.root = root.left;
        return t;
    }

    // precondición: árbol distino de vacío
    public BinaryTreeSearch getRight(){
        BinaryTreeSearch t = new BinaryTreeSearch();
        t.root = root.right;
        return t;
    }

    // precondicion: -
    public boolean ocurr(java.lang.Comparable x){
            return exist(root, x);
        }

    // precondicion: árbol distinto de vacío
    public Object getMin(){
            return getMin(root).elem;
        }

    // precondicion: árbol distinto de vacío
    public Object getMax(){
            return getMax(root).elem;
        }

    // precondicion: elemento a find pertenece al arbol
    public Object find(java.lang.Comparable x){
            return find(root, x).elem;
        }

    // precondicion: elemento a insert no pertenece al árbol
    public void insert(java.lang.Comparable x){
            root = insert(root, x);
        }


    // precondicion: elemento a remove pertenece al árbol
    public void remove(java.lang.Comparable x){
            root = remove(root, x);
        }



    // METODOS PRIVADOS
    private boolean exist(DoubleNode t, java.lang.Comparable x) {
            if (t == null)
                return false;
            if (x.compareTo(t.elem) == 0)
                return true;
            else if (x.compareTo( t.elem)< 0)
                return exist(t.left, x);
            else
                return exist(t.right, x);
        }

    private DoubleNode<T> getMin(DoubleNode<T> t){
            if (t.left == null)
                return t;
            else
                return getMin(t.right);
        }

    private DoubleNode<T> getMax(DoubleNode<T> t){
            if (t.right == null)
                return t;
            else
                return getMax(t.right);
        }

    private DoubleNode<T> find(DoubleNode<T> t, java.lang.Comparable x){
            if (x.compareTo( t.elem)== 0)
                return t;
            else if (x.compareTo( t.elem)< 0)
                return find(t.left, x);
            else
                return find(t.right, x);
        }

    private DoubleNode<T> insert(DoubleNode<T> t, java.lang.Comparable x) {
            if (t == null){
                t = new DoubleNode<T>((T) x);
                t.elem = (T) x;
            }
            else if (x.compareTo(t.elem) < 0)
                t.left = insert(t.left, x);
            else
                t.right = insert(t.right, x);
            return t;
        }

    private DoubleNode remove(DoubleNode t, Comparable x) {
            if (x.compareTo(t.elem) < 0)
                t.left = remove(t.left, x);
            else if (x.compareTo(t.elem) > 0)
                t.right = remove(t.right, x);
            else
            if (t.left != null && t.right != null ) {
                t.elem = getMin(t.right).elem;
                t.right = removeMin(t.right);
            }
            else if (t.left != null)
                t = t.left;
            else
                t =t.right;
            return t;
        }

    private DoubleNode removeMin(DoubleNode t){
            if (t.left != null)
                t.left = removeMin(t.left);
            else
                t = t.right;
            return t;
        }
}

