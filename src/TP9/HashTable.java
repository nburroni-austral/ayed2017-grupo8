package TP9;

import struct.istruct.Hashable;

/**
 * Created by Lucas on 17/6/2017.
 */
public class HashTable <T> {
    // Implementacion de algunos metodos de
    // Tabla Hash con resolucion dinamica de colisiones
    // El objeto que se inserta debe implementar las interfaces Comparable y Hashable.
    // Autor Alicia Gioia

    private DynamicList<T> t[];
    private int capacity;

    public HashTable(int M) {
        if (!Prime.isPrime(M))
            M = Prime.nextPrime(M);
        capacity = M;
        t = new  DynamicList[M];
        for(int i = 0; i < M ; i++)
            t[i] = new DynamicList<T>();
    }

    public void insert(T x) {
        int k =((Hashable) x).hashCode(capacity);
        t[k].insertNext(x);
    }

    public T find(T x) {
        int k = ((Hashable) x).hashCode(capacity);
        t[k].goTo(0);
        int l = t[k].size();
        for (int i = 0 ; i < l ; i ++ )
            if (((Comparable) x).compareTo(t[k].getActual())== 0)
                return t[k].getActual();
        return x;
    }

    public boolean occur(T x) {
        int k = ((Hashable) x).hashCode(capacity);
//        t[k].goTo(0);
        int l = t[k].size();
        for (int i = 0 ; i < l ; i ++ ) {
            t[k].goTo(i);
            if (((Comparable) x).compareTo(t[k].getActual()) == 0)
                return true;
        }
        return false;
    }

    public DynamicList<T> getDynamicList(int hashcode){
        if (hashcode<=capacity)
            return t[hashcode];
        else
            return new DynamicList<T>();
    }

    public int getCapacity() {
        return capacity;
    }

    public BinaryTreeSearch<T> getBinaryTreeSearch() {
        BinaryTreeSearch<T> a = new BinaryTreeSearch<T>();
        for (int i = 0; i < capacity; i++ ) {
            if (!t[i].isVoid()) {
                t[i].goTo(0);
                for (int j = 0; j < t[i].size() ; j++) {
                    a.insert((Comparable) t[i].getActual());
                    t[i].goNext();
                    j++;
                }
            }
        }
        return a;
    }

}
