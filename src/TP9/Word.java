package TP9;

import struct.istruct.Hashable;

/**
 * Created by Lucas on 8/7/2017.
 */
public class Word implements Hashable,Comparable<Word> {
    private String word;
    private String soundexWord;


    public Word(String word, String soundexWord) {
        this.word = word;
        this.soundexWord = soundexWord;

    }

    @Override
    public int compareTo(Word w) {
        if(w.word.equals(word))
            return 0;
        else
            return -1;
    }

    @Override
    public int hashCode(int tableSize) {
        if (soundexWord.length()>1) {
            String numbers = "";
            int aux;
            for (int i = 1; i < soundexWord.length(); ++i) {
                aux = soundexWord.charAt(i);
                numbers += aux;
            }
            return (soundexWord.charAt(0) * Integer.parseInt(numbers)) % tableSize;
        }else {
            return soundexWord.charAt(0) % tableSize;
        }
    }

    public String getString() {
        return word;
    }

    public String getSoundexWord() {
        return soundexWord;
    }
}
