package TP9;

import java.io.*;

/**
 * Created by Lucas on 17/6/2017.
 */
public class WordReviser {
    private HashTable<Word> dictionary;

    /**
     * Description: Constructor adds all the words in file to HashTable.
     * Precondition: File dictionary has only one word (english word) per line and capacityHashSize represents
     * the numbers of words.
     * Postcondition: HashTable initialized.
     * @param dictionary
     */
    public WordReviser(File dictionary,int capacityHashSize){
        try {
            this.dictionary = new HashTable<Word>(capacityHashSize+(capacityHashSize*20/100));

            FileReader fileReader = new FileReader(dictionary);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String str = bufferedReader.readLine();
            while (str!= null){
                str=str.toUpperCase();
                this.dictionary.insert(new Word((str), soundexValue(str)));
                str = bufferedReader.readLine();
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Description: analise for unrecognized word.
     */
    public void reviseFile(File text){
        try {
            FileReader fileReader = new FileReader(text);
            BufferedReader br = new BufferedReader(fileReader);
            String str = br.readLine();

            while (str!= null){
                //noinspection ResultOfMethodCallIgnored
                str=str.toUpperCase();
                int index=0;
                for (int i=0;i<str.length();++i){
                    if (str.charAt(i)<'A' || str.charAt(i)>'Z'){
                        String word= str.substring(index,i);
                        if (word.length()>0) {
                            soundexMatch(new Word(word, soundexValue(word)));
                        }
                        index = i + 1;

                    }
                }
                str=br.readLine();
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Description: Private method which suggest, if it's the string not found, the most representatives word
     * by using soundex algorithm key.
     * @param word
     */

    private void soundexMatch(Word word) {
        if (!dictionary.occur(word)){
            DynamicList<Word> suggestions = dictionary.getDynamicList(word.hashCode(dictionary.getCapacity()));
            if (!suggestions.isVoid()){
                System.out.println("'"+word.getString()+"' not match. Yours suggestions:");
                for (int i=0; i<suggestions.size();++i){
                    suggestions.goTo(i);
                    if (word.getSoundexWord().equals(suggestions.getActual().getSoundexWord()))
                        System.out.println("\t"+suggestions.getActual().getString());
                }
            }
        }
    }

    /**
     * Description: Private method which estimates soundex key or value for a particular word.
     * @param string
     */
    private String soundexValue(String string) {
        char firstLetter = string.charAt(0);
        String result = "" + firstLetter;
        try {
            int stop = 3;
            int index = 1;
            while (index < string.length() && stop > 0) {
                char c = string.charAt(index);
                if (c == 'B' || c == 'F' || c == 'P' || c == 'V') {
                    result += '1';
                    --stop;
                } else if (c == 'C' || c == 'G' || c == 'K' || c == 'Q' || c == 'S' || c == 'X' || c == 'Z') {
                    result += '2';
                    --stop;
                } else if (c == 'D' || c == 'T') {
                    result += '3';
                    --stop;
                } else if (c == 'L') {
                    result += '4';
                    --stop;
                } else if (c == 'M' || c == 'N') {
                    result += '5';
                    --stop;
                } else if (c == 'R') {
                    result += '6';
                    --stop;
                }
                index++;
            }
        } catch (StringIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
