package TP9;

import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Lucas on 8/7/2017.
 */
public class WordReviserTest {
    public static void main(String[] args){
        File dictionary = new File("/Users/santiagohazana/Documents/Austral/AYED/ayed2017-grupo8/src/TP9/Dictionary");
        WordReviser wordReviser = new WordReviser(dictionary,3000);

        File fileTxt = new File("Text");
        try {
            FileWriter fileWriter = new FileWriter(fileTxt);

            fileWriter.write("ticher-");
            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        wordReviser.reviseFile(fileTxt);

    }
}
