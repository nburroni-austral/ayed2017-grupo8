package TPCaballo.controller;

import TPCaballo.model.Cell;
import TPCaballo.model.DynamicStack;
import TPCaballo.model.KnightMovement;
import TPCaballo.view.KnightSwing;
import TPCaballo.view.StartWindow;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Description: game controller, creates the window and knightMovement model and administrates it
 */
public class KnightMovementController implements ActionListener {

    private KnightSwing window; // window for knight visualization
    private KnightMovement knightMovement;
    private StartWindow startWindow;

    /**
     * Constructor: initializes the window and the knight movement, also checks the first possible movements and paints the used cell and possible cells
     */

    public KnightMovementController(){
        startWindow = new StartWindow(this);
    }

    /**
     * Description: starts a new game, initializies a new window and knight movement and painting the first cells
     */

    private void startGame() {
        window = new KnightSwing(this);
        knightMovement = new KnightMovement();
        knightMovement.checkPossibleMovements();
        paintUsedCell();
        paintPossibleMovements();
    }

    /**
     * Description: checks what button is pressed and calls nextMovement when next movement is pressed.
     */

    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();

        if(button.getText() == "Next movement"){
            System.out.println("Next movement");
            nextMovement();

        }else if(button.getText() == "Exit Game"){
            System.exit(0);
        }else if(button.getText() == "Exit"){
            window.setVisible(false);
            startWindow.setVisible(true);
        }else if(button.getText() == "Start Game"){
            startWindow.setVisible(false);
            startGame();
            window.setVisible(true);
        }
    }

    /**
     * Description: performs the next movement and painting of the cells
     */

    private void nextMovement() {
        markAsUsed();
        knightMovement.chooseMovement();
        rePaintBoard();
        paintUsedCell();
        knightMovement.empty();
        knightMovement.checkPossibleMovements();
        paintPossibleMovements();
    }

    /**
     * Description: calls the method paintPossibleMovements from window copying the possible movement stack so that is not affected by the method
     */

    private void paintPossibleMovements(){
        DynamicStack<Cell> toPaintStack = new DynamicStack<Cell>();
        while(!knightMovement.possibleMovements.isEmpty()) {
            toPaintStack.push(knightMovement.possibleMovements.peek());
            knightMovement.possibleMovements.pop();
        }
        while (!toPaintStack.isEmpty()) {
            Cell cell = toPaintStack.peek();
            window.paintPossibleMovements(getTile(cell), cell.getRow(), cell.getASCII());
            knightMovement.possibleMovements.push(cell);
            toPaintStack.pop();
        }

    }

    /**
     * Description: calls the method rePaintBoard from window copying the possible movement stack so that is not affected by the method
     */

    private void rePaintBoard(){
        DynamicStack<Cell> toPaintStack = new DynamicStack<Cell>();
        while(!knightMovement.possibleMovements.isEmpty()) {
            toPaintStack.push(knightMovement.possibleMovements.peek());
            knightMovement.possibleMovements.pop();
        }
        while (!toPaintStack.isEmpty()) {
            Cell cell = toPaintStack.peek();
            window.rePaintBoard(getTile(cell));
            knightMovement.possibleMovements.push(cell);
            toPaintStack.pop();
        }
    }

    /**
     * Description: calls the method paintUsedCellAndMarkMovement from window
     */

    private void paintUsedCell(){
        window.paintUsedCellAndMarkMovement(getTile(knightMovement.usedCells.peek()), knightMovement.usedCells.peek().getMovement());
    }

    /**
     * Description: calls the method markAsUsed from window
     */

    private void markAsUsed(){
        window.markAsUsed(getTile(knightMovement.usedCells.peek()));
    }

    /**
     * Description: return the number of tile of a cell
     */

    private int getTile(Cell cell){
        int row = cell.getRow();
        int column = cell.getColumn();
        return (8*row)+column;
    }
}
