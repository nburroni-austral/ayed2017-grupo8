package TPCaballo.model;

/**
 * Description: Cell object for the chess board
 */
public class Cell {
    private int row;
    private int column;
    private boolean used = false;
    int movement = 0;

    /**
     *Constructor: creates a new cell with the given row and column parameter
     */

    public Cell(int row, int column){
        this.row = row;
        this.column = column;
    }

    /**
     * Description: marks the cell as used when the knight steps on it
     */

    public void used(){
        used = true;
    }

    public boolean isUsed(){
        return used;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    public int getMovement() {
        return movement;
    }

    /**
     * Description: returns the ASCII value of the column, used to print the letter corresponding to the column
     */

    public int getASCII(){
        for (int i = 0; i < 8; i++) {
            if(column == i){
                return 65+i;
            }
        }
        return -1;
    }

}
