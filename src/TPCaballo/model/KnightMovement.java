package TPCaballo.model;

import java.util.Random;

/**
 * Description: handles all the logic for the knight movement
 */
public class KnightMovement {
    Random rn = new Random(); // random generator for random choices
    int numberOfMovements = 1; // keeps track of the number of movements that the knight makes
    public DynamicStack<Cell> usedCells = new DynamicStack(); // keeps track of the cells that the knight has already been
    public DynamicStack<Cell> possibleMovements = new DynamicStack(); // stack of possible movements of the knight, resets every turn

    private Cell[][] board = new Cell[8][8];

    /**
     * Description: constructor, fills the board with cells and assigns a random starting position for the knight
     */

    public KnightMovement(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                board[i][j] = new Cell(i, j);
            }
        }
        int xPos = rn.nextInt(8);
        int yPos = rn.nextInt(8);
        board[xPos][yPos].used();
        board[xPos][yPos].setMovement(numberOfMovements);
        usedCells.push(board[xPos][yPos]);
    }

    /**
     * Description: prints the board in console, telling if a cell was used or not. Used for debugging and testing
     */

    public void printBoard(){
        System.out.println("     |   A   |   B   |   C   |   D   |   E   |   F   |   G   |   H   |");
        System.out.println(" ----+-------+-------+-------+-------+-------+-------+-------+-------+");
        for (int i = 0; i < 8; i++) {
            System.out.print("   "+ (i+1) +" | ");
            for (int j = 0; j < 8; j++) {
                if(board[i][j].isUsed()) {
                    System.out.print(board[i][j].isUsed() + " " + board[i][j].getMovement() + "| ");
                }else{
                    System.out.print(board[i][j].isUsed() + " | ");
                }
            }
            System.out.println("\n ----+-------+-------+-------+-------+-------+-------+-------+-------+");
        }
    }

    /**
     * Description: checks the possible movements that the knight can make, except the cells that it has already visited
     */

    public void checkPossibleMovements() {
        Cell lastCell = usedCells.peek();
        int fromRow = lastCell.getRow();
        int fromColumn = lastCell.getColumn();

        if(fromRow + 2 < 8 && fromColumn - 1 >= 0){
            if(!board[fromRow + 2][fromColumn - 1].isUsed()){
                possibleMovements.push(board[fromRow + 2][fromColumn - 1]);
            }
        }
        if(fromRow + 2 < 8 && fromColumn + 1 < 8){
            if(!board[fromRow + 2][fromColumn + 1].isUsed()) {
                possibleMovements.push(board[fromRow + 2][fromColumn + 1]);
            }
        }
        if(fromRow + 1 < 8 && fromColumn - 2 >= 0){
            if(!board[fromRow + 1][fromColumn - 2].isUsed()) {
                possibleMovements.push(board[fromRow + 1][fromColumn - 2]);
            }
        }
        if(fromRow + 1 < 8 && fromColumn + 2 < 8){
            if(!board[fromRow + 1][fromColumn + 2].isUsed()){
                possibleMovements.push(board[fromRow + 1][fromColumn + 2]);
            }
        }
        if(fromRow - 1 >= 0 && fromColumn - 2 >= 0){
            if(!board[fromRow - 1][fromColumn - 2].isUsed()){
                possibleMovements.push(board[fromRow - 1][fromColumn - 2]);
            }
        }
        if(fromRow - 1 >= 0 && fromColumn + 2 < 8){
            if(!board[fromRow - 1][fromColumn + 2].isUsed()){
                possibleMovements.push(board[fromRow - 1][fromColumn + 2]);
            }
        }
        if(fromRow - 2 >= 0 && fromColumn - 1 >= 0){
            if(!board[fromRow - 2][fromColumn - 1].isUsed()){
                possibleMovements.push(board[fromRow - 2][fromColumn - 1]);
            }
        }
        if(fromRow - 2 >= 0 && fromColumn + 1 < 8){
            if(!board[fromRow - 2][fromColumn + 1].isUsed()){
                possibleMovements.push(board[fromRow - 2][fromColumn + 1]);
            }
        }
    }

    /**
     * Description: chooses a random movement from the possible movements
     */

    public void chooseMovement(){
        DynamicStack<Cell> tmpStack = new DynamicStack<Cell>();
        while(!possibleMovements.isEmpty()) {

            tmpStack.push(possibleMovements.peek());
            possibleMovements.pop();
        }

        if(tmpStack.size() == 0){
            System.out.println("No possible movement");
            return;
        }
        int option = rn.nextInt(tmpStack.size())+1;
        while(tmpStack.size() != option){
            possibleMovements.push(tmpStack.peek());
            tmpStack.pop();
        }
        int row = tmpStack.peek().getRow();
        int column = tmpStack.peek().getColumn();
        board[row][column].used();
        usedCells.push(tmpStack.peek());
        numberOfMovements++;
        board[row][column].setMovement(numberOfMovements);

        while(!tmpStack.isEmpty()) {

            possibleMovements.push(tmpStack.peek());
            tmpStack.pop();
        }
    }

    /**
     * Description: empties the possible movements
     */

    public void empty(){
        possibleMovements.empty();
    }
}
