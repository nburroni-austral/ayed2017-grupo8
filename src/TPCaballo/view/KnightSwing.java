package TPCaballo.view;

import java.awt.*;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * Description: call that creates the window and paints the cells
 */

public class KnightSwing extends JFrame{
    static int N = 8;

    JPanel board = new JPanel();

    /**
     * Constructor: creates the window with the chess board cells and two
     * buttons for nex movement and exit, also receives an action listener to set ir to the buttons
     */

    public KnightSwing(ActionListener listener) {
        this.setTitle("Knight movement");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);

        for (int i = 0; i < N * N; i++) {
            JButton tile = new JButton();
            tile.setOpaque(true);
            tile.setBorderPainted(false);
            if ((i / N + i % N) % 2 == 1) {
                tile.setBackground(Color.black);
            }else{
                tile.setBackground(Color.white);
            }
            tile.setPreferredSize(new Dimension(75, 75));
            board.add(tile);
        }

        JButton nextMovement = new JButton("Next movement");
        nextMovement.addActionListener(listener);
        board.add(nextMovement, Component.BOTTOM_ALIGNMENT);

        JButton exit = new JButton("Exit");
        exit.addActionListener(listener);
        board.add(exit, Component.BOTTOM_ALIGNMENT);

        this.add(board);
        this.pack();
        this.setSize(700, 700);
        this.setLocationRelativeTo(null);
    }

    /**
     * Description: paints the cell that the knight is currently at and
     * sets the number of movement to the cell
     */

    public void paintUsedCellAndMarkMovement(int tile, int numberOfMovement){
        board.getComponent(tile).setBackground(Color.red);
        ((JButton)board.getComponent(tile)).setText(String.valueOf(numberOfMovement));
    }

    /**
     * Description: paints the given cell in orange to mark as used before
     */

    public void markAsUsed(int tile) {
        board.getComponent(tile).setBackground(Color.orange);
    }

    /**
     * Description: paints the possible movement that the knight can make, excepting the
     * cells that it has already visited and labels the cell
     */

    public void paintPossibleMovements(int tile, int row, int column){
        board.getComponent(tile).setBackground(Color.green);
        ((JButton)board.getComponent(tile)).setText(Character.toString((char)column) + String.valueOf(row));
    }

    /**
     * Description: paints back the cells to the normal board color (black or white) that were
     * previously painted in paintPossibleMovements, removing the label
     */

    public void rePaintBoard(int tile){
        boolean isBlack = (tile / N + tile % N) % 2 == 1;
        board.getComponent(tile).setBackground(isBlack? Color.black : Color.white);
        ((JButton)board.getComponent(tile)).setText("");
    }
}