package TPCaballo.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.net.URL;

/**
 * Description: creates a start window to start or exit the game
 */
public class StartWindow extends JFrame {

    /**
     * Constructor: creates the start window with the members of the group, an
     * image and two buttons to start and exit the game
     */

    public StartWindow(ActionListener listener){

        this.setTitle("TP movimiento caballo");
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        this.add(panel);

        JLabel textMembers = new JLabel("Lucas Manzanelli, Santiago Hazaña");
        textMembers.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(textMembers);

        URL url = getClass().getResource("Square-with-knight.png");
        JLabel image = new JLabel(new ImageIcon(url));
        image.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(image, BorderLayout.NORTH);

        JButton startGameButton = new JButton("Start Game");
        startGameButton.setSize(150, 40);
        startGameButton.setMaximumSize(startGameButton.getSize());
        startGameButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        startGameButton.addActionListener(listener);
        panel.add(startGameButton);

        JButton exitButton = new JButton("Exit Game");
        exitButton.setSize(150, 40);
        exitButton.setMaximumSize(exitButton.getSize());
        exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitButton.addActionListener(listener);
        panel.add(exitButton);

        this.setVisible(true);
        this.pack();
        this.setSize(250, 200);
        this.setLocationRelativeTo(null);//aligned to the center of the screen
    }

}
