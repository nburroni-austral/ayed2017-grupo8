package TPDiseñoPorContratos;

/**
 * Created by santiagohazana on 3/14/17.
 */
public class Colectivo implements Comparable{
    private /*@ non null @*/ int linea;
    //@ invariant linea > 0;
    private /*@ non null @*/ int interno;
    //@ invariant linea > 0;
    private /*@ non null @*/ int asientos;
    //@ invariant asientos > 0;
    boolean discpacitados;


    /*@
        requires linea > 0, interno > 0, asientos > 0;
        assignable linea, interno, asientos, discapacitados;
     @*/
    public Colectivo(int  linea, int interno, int asientos, boolean discpacitados) {
        this.linea = linea;
        this.interno = interno;
        this.asientos = asientos;
        this.discpacitados = discpacitados;
    }

    /*@
        requires linea > 0, interno > 0;
        assignable linea, interno;
     @*/

    public Colectivo(int linea, int interno) {
        this.linea = linea;
        this.interno = interno;
    }

    public int compareTo(Object x){
        Colectivo y = (Colectivo) x;

        if (this.linea != y.linea) {
            return this.linea - y.linea;
        }else {
            return this.interno - y.interno;
        }

    }

    public /*@ pure @*/ int getLinea() {
        return linea;
    }

    /*@ requires linea > 0;
        assignable linea;
        ensures linea != \old (linea);
     @*/

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public /*@ pure @*/ int getInterno() {
        return interno;
    }

    /*@ require interno > 0;
        assignable interno;
        ensures interno != \old (interno);
     @*/

    public void setInterno(int interno) {
        this.interno = interno;
    }

    public /*@ pure @*/ int getAsientos() {
        return asientos;
    }

    /*@ requires asientos > 0;
        assignable asientos;
        ensures asientos != \old (asientos);
     @*/

    public void setAsientos(int asientos) {
        this.asientos = asientos;
    }

    public /*@ pure @*/ boolean isDiscpacitados() {
        return discpacitados;
    }

    //@ ensures discpacitados != \old (discpacitados);

    public void setDiscpacitados(boolean discpacitados) {
        this.discpacitados = discpacitados;
    }



}

