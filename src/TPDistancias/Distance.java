package TPDistancias;

/**
 * Created by Lucas on 16/3/2017.
 */
public class Distance {

    public static int hammingDistance(String wordA, String wordB){

        int distance = 0;

        if(wordA.length()>=wordB.length()) {
            for (int i = 0; i < wordB.length();i++) {
                if (wordA.toLowerCase().charAt(i) != wordB.toLowerCase().charAt(i))
                    distance++;
            }
            distance+=(wordA.length()-wordB.length());

        }else {
            for (int j=0;j<wordA.length();j++){
                if(wordA.toLowerCase().charAt(j)!=wordB.toLowerCase().charAt(j))
                    distance++;
            }
            distance+=(wordB.length()-wordA.length());
        }

        return distance;
    }

    public static int levenshteinDistance(String word, String wordToChange){
        int operations = 0;
        if(word.length() == wordToChange.length()) {
            for (int i = 0; i < word.length(); i++) {
                if(word.toLowerCase().charAt(i) != wordToChange.toLowerCase().charAt(i)){
                    operations++;
                }
            }
        }

        operations += hammingDistance(word, wordToChange);

        return operations;
    }
}

