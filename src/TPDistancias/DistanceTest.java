package TPDistancias;

/**
 * Created by Lucas on 16/3/2017.
 */
public class DistanceTest {
    public static void main(String[] args){
        String lucas = "Lucas";
        String santiago = "Santiago";
        String nicolas = "NICOLAS";

        System.out.println("Distance between "+ lucas +" and "+ lucas +" ="+Distance.hammingDistance(lucas,lucas));

        System.out.println("Distance between "+ lucas +" and "+ santiago +" ="+Distance.hammingDistance(lucas,santiago));

        System.out.println("Distance between "+ santiago +" and "+ lucas +" ="+Distance.hammingDistance(santiago,lucas));

        System.out.println("Distance between "+ santiago +" and "+ nicolas +" ="+Distance.hammingDistance(santiago,nicolas));

        System.out.println("Distance between "+ nicolas +" and "+ santiago +" ="+Distance.hammingDistance(nicolas,santiago));

        System.out.println("Number of operation between: " + lucas + " and " + lucas + " = " + Distance.levenshteinDistance(lucas, lucas));

        System.out.println("Number of operation between: " + lucas + " and " + santiago + " = " + Distance.levenshteinDistance(lucas, santiago));

        System.out.println("Number of operation between: " + lucas + " and " + nicolas + " = " + Distance.levenshteinDistance(lucas, nicolas));

        System.out.println("Number of operation between: " + santiago + " and " + nicolas + " = " + Distance.levenshteinDistance(santiago, nicolas));
    }
}
