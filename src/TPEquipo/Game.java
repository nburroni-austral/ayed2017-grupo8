package TPEquipo;

/**
 * Created by Lucas on 15/4/2017.
 */
public class Game {
    private Team localTeam;
    private Team visitorTeam;

    private int scoreLocalTeam;
    private int scoreVisitorTeam;

    public Game(Team localTeam, Team visitorTeam) {
        this.localTeam = localTeam;
        this.visitorTeam = visitorTeam;
        this.scoreLocalTeam =0;
        this.scoreVisitorTeam =0;
    }


    public void setScoresResults(int insertScoreLocalTeam, int insertScoreVisitorTeam){
        scoreLocalTeam=insertScoreLocalTeam;
        scoreVisitorTeam=insertScoreVisitorTeam;
    }

    public Team getLocalTeam() {
        return localTeam;
    }

    public Team getVisitorTeam() {
        return visitorTeam;
    }

    public int getScoreLocalTeam() {
        return scoreLocalTeam;
    }

    public int getScoreVisitorTeam() {
        return scoreVisitorTeam;
    }

    public char getResult(){
        if(scoreLocalTeam == 3){
            return '1';
        }else if (scoreLocalTeam == 0){
            return '2';
        }else{
            return 'X';
        }
    }
}
