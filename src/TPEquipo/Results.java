package TPEquipo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Lucas on 15/4/2017.
 */
public class Results {

    int numberOfTeams;
    int numberOfGames;
    private HashMap<String, Team> teams;
    private Game[] games;

    private HashMap<String,Team> teamsAux;

    public Results(){
        this.teams = new HashMap<String, Team>();
        this.games= new Game[18];
//        askForTable();
        teams.put("Deportivo", new Team("Deportivo", 11));
        teams.put("Betis", new Team("Betis", 9));
        teams.put("Sevilla", new Team("Sevilla", 6));
        teams.put("AtlMadrid", new Team("AtlMadrid", 6));
        teams.put("Barcelona", new Team("Barcelona", 5));
        teams.put("AtlBilbao", new Team("AtlBilbao", 4));
        teams.put("Madrid", new Team("Madrid", 2));
        teams.put("Espanyol", new Team("Espanyol", 2));
        teams.put("Valencia", new Team("Valencia", 1));
        teams.put("RealSociedad", new Team("RealSociedad", 1));

        games[0] = new Game(teams.get("Deportivo"), teams.get("RealSociedad"));
        games[1] = new Game(teams.get("Barcelona"), teams.get("AtlMadrid"));
        games[2] = new Game(teams.get("AtlBilbao"), teams.get("Espanyol"));
        games[3] = new Game(teams.get("AtlMadrid"), teams.get("Madrid"));
        games[4] = new Game(teams.get("Deportivo"), teams.get("Madrid"));
        games[5] = new Game(teams.get("Betis"), teams.get("Deportivo"));
        games[6] = new Game(teams.get("RealSociedad"), teams.get("Espanyol"));
        games[7] = new Game(teams.get("Valencia"), teams.get("Deportivo"));
        games[8] = new Game(teams.get("Deportivo"), teams.get("Barcelona"));
        games[9] = new Game(teams.get("Madrid"), teams.get("Barcelona"));
        games[10] = new Game(teams.get("Espanyol"), teams.get("Sevilla"));
        games[11] = new Game(teams.get("Sevilla"), teams.get("AtlMadrid"));
        games[12] = new Game(teams.get("Madrid"), teams.get("Betis"));
        games[13] = new Game(teams.get("Valencia"), teams.get("AtlBilbao"));
        games[14] = new Game(teams.get("Betis"), teams.get("AtlBilbao"));
        games[15] = new Game(teams.get("Valencia"), teams.get("AtlMadrid"));
        games[16] = new Game(teams.get("RealSociedad"), teams.get("Betis"));
        games[17] = new Game(teams.get("Barcelona"), teams.get("Betis"));

        this.teamsAux= new HashMap<String, Team>();
        solveResult();
    }

    public void askForTable(){
        Scanner sn = new Scanner(System.in);
        System.out.println("Enter the number of teams:");
        numberOfTeams = sn.nextInt();
        System.out.println("Enter the number of games:");
        numberOfGames = sn.nextInt();
        System.out.println();
        for (int i = 0; i < numberOfTeams; i++) {
            System.out.println("Enter team name: ");
            String teamName = sn.next().toLowerCase();
            System.out.println("Enter " + teamName + " points: ");
            int teamPoints = sn.nextInt();
            teams.put(teamName, new Team(teamName, teamPoints));
        }
        for (int i = 0; i < numberOfGames; i++) {
            games = new Game[numberOfGames];
            System.out.println("Enter the local team of game " + (i+1));
            String visitorTeam = sn.next().toLowerCase();
            String localTeam = sn.next().toLowerCase();
            System.out.println("Enter the visitor team of game " + (i+1));

            games[i] = new Game(teams.get(localTeam), teams.get(visitorTeam));
        }
    }

    public void solveResult(){
        setTeamsAux();
        int index = 0;
        while (index < games.length) {
            solveResult(index);
            index++;
        }
    }

    private void solveResult(int index) {
        Team local = games[index].getLocalTeam();
        Team visitor = games[index].getVisitorTeam();

        if(checkValidGameScore(index, 3, 0) && checkScoreAndGame(local, visitor, 3, 0)){

            setResultToTeam(local,visitor,3,0);
            games[index].setScoresResults(3,0);

        }else if (checkValidGameScore(index, 0, 3) && checkScoreAndGame(local, visitor, 0, 3)) {

            setResultToTeam(local, visitor, 0, 3);
            games[index].setScoresResults(0, 3);


        } else if (checkValidGameScore(index, 1, 1) && checkScoreAndGame(local, visitor, 1, 1)) {

            setResultToTeam(local, visitor, 1, 1);
            games[index].setScoresResults(1, 1);

        }else if(index>0){
//            if(games[index].getScoreLocalTeam()!=-1 && games[index].getScoreVisitorTeam()!=-1)
                undoSetResultToTeam(local,visitor, games[index].getScoreLocalTeam(), games[index].getScoreVisitorTeam());
            solveResult(index-1);
        }
    }

    private boolean checkValidGameScore(int index, int scoreLocalIncrement, int scoreVisitorIncrement) {
        if(games[index].getScoreLocalTeam()!=scoreLocalIncrement || games[index].getScoreVisitorTeam()!= scoreVisitorIncrement){
//            undoSetResultToTeam(games[index].getLocalTeam(),games[index].getVisitorTeam(),scoreLocalIncrement,scoreVisitorIncrement);
            return true;
        }
    return false;
    }

    private void setTeamsAux() {
        ArrayList<Team> teams2 = new ArrayList<Team>(teams.values());
        String name;
        for(int i=0; i<teams2.size();i++){
            name = teams2.get(i).getName();
            teamsAux.put(name, new Team(name,0));
            teamsAux.get(name).numberOfGamesPlayedToCero();
        }
    }

    private void setResultToTeam(Team local, Team visitor, int scoreLocalIncrement, int scoreVisitorIncrement){
        teamsAux.get(local.getName()).addScore(scoreLocalIncrement);

        teamsAux.get(visitor.getName()).addScore(scoreVisitorIncrement);

    }

    private void undoSetResultToTeam(Team local, Team visitor, int scoreLocalIncrement, int scoreVisitorIncrement){
        teamsAux.get(local.getName()).undoAddScore(scoreLocalIncrement);
        teamsAux.get(visitor.getName()).undoAddScore(scoreVisitorIncrement);
    }

    private boolean checkScoreAndGame(Team local, Team visitor, int scoreLocalIncrement, int scoreVisitorIncrement){
        if(teamsAux.get(local.getName()).getScore()+ scoreLocalIncrement <= teams.get(local.getName()).getScore()){
            if (teamsAux.get(visitor.getName()).getScore()+ scoreVisitorIncrement <= teams.get(visitor.getName()).getScore() ){
                return true;
            }
        }
        return false;
    }

    private void printResult(){
        for (int i = 0; i < games.length; i++) {
            System.out.print(games[i].getResult()+" ");
        }
    }

    public static void main(String[] args) {
        Results soccerTable = new Results();
        soccerTable.printResult();
    }
}
