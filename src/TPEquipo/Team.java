package TPEquipo;

/**
 * Created by Lucas on 15/4/2017.
 */
public class Team{
    private String name;
    private int score;
    private int numberOfGamesPlayed;
//    private int gamesWon;
//    private int gamesTie;
//    private int gamesLost;

    public Team(String name, int score) {
        this.name = name;
        this.score = score;
        this.numberOfGamesPlayed =0;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void addNumberOfMatches(){
        numberOfGamesPlayed++;
    }

    public void substractNumberOfMatches(){
        numberOfGamesPlayed--;
    }

    public void numberOfGamesPlayedToCero() {
        this.numberOfGamesPlayed = 0;
    }

    public int getNumberOfGamesPlayed() {
        return numberOfGamesPlayed;
    }

    public void addScore(int i) {
        score += i;
    }

    public void undoAddScore(int i){
        score = score - i;
    }
//
//    public void addGamesLost(int gamesLost) {
//        this.gamesLost = gamesLost;
//    }
//
//    public void setGamesTie(int gamesTie) {
//        this.gamesTie = gamesTie;
//    }
//
//    public void setGamesWon(int gamesWon) {
//        this.gamesWon = gamesWon;
//    }
//
//    public int getGamesLost() {
//        return gamesLost;
//    }
//
//    public int getGamesTie() {
//        return gamesTie;
//    }
//
//    public int getGamesWon() {
//        return gamesWon;
//    }
}
