package TPSudoku;

import TP3Pila.Ejercicio1.DynamicStack;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Lucas on 30/3/2017.
 */
public class Sudoku {
    private  DynamicStack<Integer> auxMemory;
    private int[][] matriz;
    private boolean[][] originalNumbers;
    private DynamicStack<Integer> memory;

    public Sudoku() {
        this.matriz = new int[9][9];
        this.originalNumbers = new boolean[9][9];
        this.auxMemory = new DynamicStack<Integer>();
        this.memory= new DynamicStack<Integer>();

    }


    /**
     * Method to solve the soduko why using stacks.
     */
    public void solveSudokuUsingStacks(){
        for (int row=0;row<matriz.length;row++){
            refillMemoryForRow(row);
            solveBackTrackingUsingStacks(row,0);
            memory.empty();
            }
    }

    /**
     * Method that it´s based in the backtracking algorith.
     * Uses a "memory" Stack which contains all the numbers possibles in a Row and an "auxMemory" Stack which
     * is empty with the numbers that there no used in a certain position and must be necessary to complete the row with
     * the nine numbers.
     */

    public void solveBackTrackingUsingStacks(int row, int column) {
        int c = column;
        int aux=0;

        while (!memory.isEmpty()) {
            if(isThisPositionOrigin(row,c))
                c++;
            else if(!auxMemory.isEmpty() && checkGeneric(auxMemory.peek(), row, c) && auxMemory.peek()!=aux){
                matriz[row][c] = auxMemory.peek();
                auxMemory.pop();
                c++;
            }
            else if (checkGeneric(memory.peek(), row, c) && memory.peek()!=aux) {
                matriz[row][c] = memory.peek();
                memory.pop();
                c++;
            } else {
                auxMemory.push(memory.peek());
                memory.pop();
            }
        }
        if(!auxMemory.isEmpty() && c>0) {
            if(!isThisPositionOrigin(row,c-1))
            auxMemory.push(matriz[row][c-1]);
            auxMemoryToMemoryStack();
            auxMemory.push(matriz[row][c-1]);
            matriz[row][c]=0;
//            while (isThisPositionOrigin(row,c) && c>0)
//                c--;
            solveBackTrackingUsingStacks(row, c-1);

        }



    }

    /**
     * Auxiliary method to pass from the aux stack to the memory stack.
     */
    private void  auxMemoryToMemoryStack(){
        while (!auxMemory.isEmpty()){
            memory.push(auxMemory.peek());
            auxMemory.pop();
        }
    }

    /**
     * Refill the memory with all the possible numbers for the solution
     */

    private void refillMemoryForRow(int row){
        ArrayList<Integer> originNumbers = passOriginNumbersInARow(row);
        int number =9;
        while (number>0){
            if(!originNumbers.contains(number)) {
                memory.push(number);
            }
            number--;
        }
    }

    /**
     * Auxiliary method to count all the numbers setted that couldn´t be a solution.
     */
    private ArrayList<Integer> passOriginNumbersInARow(int row){
        ArrayList<Integer> originNumbers = new ArrayList<Integer>();
        for (int columb=0; columb<matriz[row].length; columb++){
            if (isThisPositionOrigin(row,columb)){
                originNumbers.add(matriz[row][columb]);
            }
        }
        return originNumbers;
    }

    public void solveSudoku(){
        for (int row=0; row<matriz.length;row++){
            for (int column=0; column<matriz[row].length;column++)
                if(!isThisPositionOrigin(row,column))
                    solveBackTracking(row,column);

        }
    }

    public void solveBackTracking(int row, int column){
        int number = matriz[row][column]+1;
        while(checkGeneric(number,row,column)==false&&number<9)
            number++;
        if(checkGeneric(number,row,column) && number<10&& !isThisPositionOrigin(row,column)) {
            matriz[row][column] = number;
        }
        else if(column>0) {
            solveBackTracking(row, column - 1);
        }
    }

    public boolean checkGeneric(int number, int row, int column){
       if(checkRow(number,column) && checkColumn(number,row) && checkBox(number, row, column))
        return true;
       return false;
    }

    /**
     * Check if the number that we pass it´s repeated in the same row, in that case return true.
     */
    private boolean checkRow(int number,int column){
        for (int i=0; i<matriz.length;i++){
            if(matriz[i][column]==number)
                return false;
        }
        return true;
    }

    /**
     * Check if the number that we pass it´s repeated in the same column, in that case return true.
     */
    private boolean checkColumn(int number, int row){
        for (int i=0; i<matriz[row].length;i++){
            if(matriz[row][i]==number)
                return false;
        }
        return true;
    }

    /**
     *Check if the number it´s repeated in the box specify with the position that we pass.
     */
    private boolean checkBox (int number, int row, int column ){
        int maxR = numberInInterval(row)+3;
        int maxC = numberInInterval(column)+3;

        for (int r=numberInInterval(row);r<(maxR);r++){
            for (int c=numberInInterval(column);c<(maxC);c++){
                if(matriz[r][c]==number){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Auxiliary method of checkBox, wich allows to limit space that must belong to an specify box.
     */

    private int numberInInterval(int numberPosition){
        int n=0;
        if(numberPosition>=3 && numberPosition<6) {
            n = 3;
        }
        else if (numberPosition>=6 && numberPosition<9) {
            n = 6;
        }
        return n;
    }

    /**
     * Method that ask if the position that we use for the matriz is an original number by using another matriz 'originNumbers'.
     * return true if the position in originNumbers is true.
     */

    public boolean isThisPositionOrigin(int row, int column){
        if(originalNumbers[row][column]) {
            return true;
        }
        return false;
    }

    /**
     * Set 17 random numbers in different positions that must be necessary for an unique resolution.
     */

    public void setRandomNumbers() {
        int count=0;
        int row;
        int column;
        int number;
        Random random = new Random();
        while (count<18){
            column=random.nextInt(9);
            row =random.nextInt(9);
            number= random.nextInt(9)+1;

            if(checkGeneric(number,row,column)){
                matriz[row][column] = number;
                originalNumbers[row][column]=true;
                count++;
            }
        }
    }

    /**
     * Set an number to the matriz in a specific position.
     */

    public boolean addNumber(int number, int row, int column){
        if(checkGeneric(number, row, column)){
            matriz[row][column] =number;
            originalNumbers[row][column]=true;
            return true;
        }
        return false;
    }

    public void addNumbersForTesting(){
        matriz[0][2]=2;
        originalNumbers[0][2]=true;
        matriz[0][3]=6;
        originalNumbers[0][3]=true;
        matriz[0][4]=8;
        originalNumbers[0][4]=true;
        matriz[1][0]=3;
        originalNumbers[1][0]=true;
        matriz[1][3]=4;
        originalNumbers[1][3]=true;
        matriz[1][6]=5;
        originalNumbers[1][6]=true;
        matriz[1][8]=9;
        originalNumbers[1][8]=true;
        matriz[2][5]=2;
        originalNumbers[2][5]=true;
        matriz[3][1]=8;
        originalNumbers[3][1]=true;
        matriz[4][4]=6;
        originalNumbers[4][4]=true;
        matriz[4][7]=9;
        originalNumbers[4][7]=true;
        matriz[4][8]=3;
        originalNumbers[4][8]=true;
        matriz[5][4]=4;
        originalNumbers[5][4]=true;
        matriz[5][5]=9;
        originalNumbers[5][5]=true;
        matriz[6][7]=7;
        originalNumbers[6][7]=true;
        matriz[7][3]=3;
        originalNumbers[7][3]=true;
        matriz[8][1]=1;
        originalNumbers[8][1]=true;

    }

    public void print(){
        for (int i=0;i<matriz.length;i++){
            for (int j=0;j<matriz[i].length;j++){
                System.out.print(matriz[i][j]+"\t");
            }
            System.out.println();

        }
    }

    public int getPosition(int row, int columnb) {
        return matriz[row][columnb];
    }
}
