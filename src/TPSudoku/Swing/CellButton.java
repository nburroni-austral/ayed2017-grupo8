package TPSudoku.Swing;

import javax.swing.*;

/**
 * Created by Lucas on 4/4/2017.
 */
public class CellButton extends JButton {

    private int row;
    private int columb;

    public CellButton(int row, int columb) {
        super();
        this.row = row;
        this.columb = columb;
    }

    public int getRow() {
        return row;
    }

    public int getColumb() {
        return columb;
    }
}
