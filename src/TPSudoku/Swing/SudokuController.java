package TPSudoku.Swing;

import TPSudoku.Sudoku;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Lucas on 1/4/2017.
 */
public class SudokuController implements ActionListener{
    private SudokuFrame sudokuFrame;
    private Sudoku sudoku;
    private SudokuSolutionFrame solutionFrame;

    public SudokuController() {
        sudokuFrame = new SudokuFrame(this);
        this.sudoku= new Sudoku();
    }

    public void actionPerformed(ActionEvent e) {
        JButton button = (JButton) e.getSource();

        if ((button instanceof CellButton) && button.isEnabled()){
            setNumber((CellButton)button);
        }

        else if(button.getText().equals("Solve Sudoku")){
            sudoku.solveSudoku();
            sudoku.print();
            showSolution();
        }
        else if(button.getText().equals("Restart")){
            starNewSudoku();
        }


        else if(button.getText().equals("Exit")){
            System.exit(0);
        }

    }

    private boolean checkNumber(String string, int row, int column){
        int number;

        if(string.length()==1 && string.charAt(0)>48 && string.charAt(0)<58) {
            number = Integer.parseInt(string.substring(0, 1));
            if(sudoku.checkGeneric(number,row,column) && !sudoku.isThisPositionOrigin(row,column)) {
                return true;
            }
        }
        return false;
    }

    private void setNumber(CellButton button){
        int row =  button.getRow();
        int columb = button.getColumb();
        String string;
        try {

            string = JOptionPane.showInputDialog("Enter a number between 1 and 9");
            if (checkNumber(string, row, columb)) {
                int number = Integer.parseInt(string.substring(0, 1));
                sudoku.addNumber(number, row, columb);
                button.setBackground(Color.lightGray);
                button.setText(string);
            } else
                JOptionPane.showMessageDialog(sudokuFrame, "Invalid number or is already set!");
        }
        catch (NullPointerException e){
            JOptionPane.showMessageDialog(sudokuFrame, "Warning nothing selected!");
        }

    }

    public void setSolutionForCellButton(CellButton button) {
        if(sudoku.isThisPositionOrigin(button.getRow(),button.getColumb()))
            button.setBackground(Color.red);
       button.setText(""+sudoku.getPosition(button.getRow(),button.getColumb()));

    }

    private void showSolution(){
        hideSudokuWindow();
        this.solutionFrame = new SudokuSolutionFrame(this);

    }

    private void hideSudokuWindow(){
        sudokuFrame.setVisible(false);
    }

    private void hideSudokuSolution(){
        solutionFrame.setVisible(false);
    }

    private void starNewSudoku(){
        hideSudokuSolution();
        this.sudokuFrame = new SudokuFrame(this);
        this.sudoku= new Sudoku();
    }
}
