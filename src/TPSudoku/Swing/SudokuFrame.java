package TPSudoku.Swing;

import TPCaballo.model.Cell;
import TPSudoku.Sudoku;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Lucas on 3/4/2017.
 */
public class SudokuFrame extends JFrame {

    private SudokuController controller;
    private JPanel panelPrincipal;
    private JPanel panelTitles;
    private JPanel  panelBoard;
    private JPanel panelButtons;
    private JLabel textMembers;
    private JButton solveSudokuButton;
    private JButton exitButton;
    private CellButton[][] cellsButton;

    public SudokuFrame(SudokuController controller) throws HeadlessException {
        super("Sudoku");

        this.controller=controller;
        this.panelPrincipal= new JPanel();
        this.panelTitles= new JPanel();
        this.panelBoard= new JPanel();
        this.panelButtons = new JPanel();
        this.textMembers = new JLabel("Lucas Manzanelli, Santiago Hazaña");
        this.solveSudokuButton = new JButton("Solve Sudoku");
        this.exitButton = new JButton("Exit");
        this.cellsButton= new CellButton[9][9];

        panelPrincipal.setLayout(new BoxLayout(panelPrincipal, BoxLayout.PAGE_AXIS));
        add(panelPrincipal);

        panelTitles.setLayout(new BoxLayout(panelTitles, BoxLayout.PAGE_AXIS));
        panelPrincipal.add(panelTitles);

        textMembers.setAlignmentX(Component.CENTER_ALIGNMENT);
        textMembers.setAlignmentY(Component.TOP_ALIGNMENT);
        panelTitles.add(textMembers);


        panelBoard.setLayout(new GridLayout(9,9));
        panelBoard.setPreferredSize(new Dimension(400,400));
        panelPrincipal.add(panelBoard);

//        for (int row = 0; row <cellsButton.length; row++) {
//            for (int column = 0; column < cellsButton[row].length; column++) {
//                JButton cellButton = new JButton();
//                cellButton.setOpaque(true);
////                cellButton.setBorderPainted(false);
//                cellButton.setBackground(Color.white);
//                cellButton.setPreferredSize(new Dimension(50,50));
////                cellButton.setBorder(new LineBorder(Color.black,14));
//                cellButton.addActionListener(this);
//                panelBoard.add(cellButton);
//            }
//        }
        for (int row = 0; row <cellsButton.length; row++) {
            for (int column = 0; column < cellsButton[row].length; column++) {
                CellButton cellButton = new CellButton(row,column);
                cellButton.setOpaque(true);
//                cellButton.setBorderPainted(false);
                cellButton.setBackground(Color.white);
                cellButton.setPreferredSize(new Dimension(75,75));
//                cellButton.setBorder(new LineBorder(Color.black,14));
                cellButton.addActionListener(controller);
                panelBoard.add(cellButton);
            }
        }

        solveSudokuButton.setSize(100, 300);
        solveSudokuButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        solveSudokuButton.addActionListener(controller);
        panelButtons.add(solveSudokuButton);

        exitButton.setSize(100, 300);
        exitButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        exitButton.addActionListener(controller);
        panelButtons.add(exitButton);

        panelPrincipal.add(panelButtons);

        this.pack();
//        this.setPreferredSize(new Dimension(600, 600));
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public CellButton getCellsButton(int row,int column) {
        return cellsButton[row][column];
    }
}
